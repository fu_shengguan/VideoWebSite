var uk ="_uk_",pk="_pk_",rk="_rk_";
$(function(){
	layui.use("layer");
	$("#loginName").focus();
	$("#loginName").val($.localStorage.get(uk));
	$("#loginPwd").val($.localStorage.get(pk));
	if($.localStorage.get(rk)){
		$("#rememberPwd").attr("checked",true);
	}
	
});





function subLogin(){
    var $msg = $("#message"),$usrname=$("#loginName"),$pwd=$("#loginPwd"),$rememberPwd = $("#rememberPwd");
    var usrname = $usrname.val()||"";


    if(usrname.trim().length==0){
        $("#tishi").html("用户名不能为空");
        return ;
    }
    var pwd = $pwd.val()||"";
    if(pwd.trim().length==0){
    	$("#tishi").html("密码不能为空!");
        return ;
    }


	$(function () {
		$('#btnLogin').on('click', function () {
			if ($('#loginName').val() == '' || $('#loginPwd').val() == '') {
				layer.msg('请输入完整信息');
				return;
			}

			$.post('login.do', {
				admname: $('#loginName').val(),
				admpwd: $('#loginPwd').val()
			}, function (data) {
				//console.log(data);

				if (data.code == 200) {
					sessionStorage.setItem('admin',JSON.stringify(data.result));
					layer.msg(data.msg, {icon: 1, time: 2000}, function () {
						location.href = "main.html";
					})
				} else {
					layer.msg(data.msg);
				}
			}, "JSON");
		})
	})

    if($rememberPwd.is(':checked')){
    	$.localStorage.set(uk, usrname);
    	$.localStorage.set(pk, pwd);
    	$.localStorage.set(rk, true);
    }else{
    	$.localStorage.remove(uk);
    	$.localStorage.remove(pk);
    	$.localStorage.remove(rk);
    }

    
    window.location.replace("main.html?menuUserName="+usrname.trim()+"&admin=0");
    
}





if(window !=top){
	top.location.href=location.href;
}


