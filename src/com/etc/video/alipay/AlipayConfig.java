package com.etc.video.alipay;

/* *
 *类名：AlipayConfig
 *功能：基础配置类
 *详细：设置帐户有关信息及返回路径
 *修改日期：2017-04-05
 *说明：
 *以下代码只是为了方便商户测试而提供的样例代码，商户可以根据自己网站的需要，按照技术文档编写,并非一定要使用该代码。
 *该代码仅供学习和研究支付宝接口使用，只是提供一个参考。
 */

public class AlipayConfig {
	
//↓↓↓↓↓↓↓↓↓↓请在这里配置您的基本信息↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓

	// 应用ID,您的APPID，收款账号既是您的APPID对应支付宝账号
	public static String app_id = "2016102700771890";
	
	// 商户私钥，您的PKCS8格式RSA2私钥
    public static String merchant_private_key = "MIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQCg5IGlxEMkQkynA2VFylFy2/SCuF1UOgt39LbBK/pHkYz67bIn2gvWqcg3iVpDuZ10pYHg6pt4H+mXiwgpScITXoiQCNZljx+AS+fl8qxGiMrjEMU4z+3fIEKaii/XsU4NJnoowQ99JPlUlfmBGRyswloGte57WGCGwv+Kxsz+gatG4VGJTw7iaJjUpdSjL0nB03yFNts4gWdhSZ0sBEtr2agjj39lFiwuTN8tkOuD09GJ5gl9JCN6tErEKYZPeAWjzYtTkj20nfRc/50LjaF6sFFhrYpfgHcPcM6ahc+YhdLouMjjUud6Lv6pLBWziENzJTmsi2RZB4lew4a3PmrNAgMBAAECggEAHa0IHMMufymbadK+gzjl/QGE//dxf2+VXfLNIqQx7bFzB3WAPsmBFX8LZvL2qzc5S6KbahjbxDReqY0DXFo2acwt9CMtKF5VuRxHEarJC9l/UpJtTiVMArO4lTiXynjxpItMTOgPK03+jxN6ic1P9K2EXWiBLE8IvtKIv9gbR3XoGYbYyWqmDNH329S41DqO+wEEbvBejtqNURo3qnoqLcPyYLvjd3gRmSvVsMOeUcIbn0a1bndLZ1pLiCY5LfpMkbtTxjv6cM7GOKfbSVu4qD9Ait6+fzvuQb4Vyx5DJtsz4kzxaGURdQHSOBNPHPyaE7/XB1Lba8C0vpIUp1c0IQKBgQDgiX0PYEAgV4LTJbgY3UPn1zrJbiuftEasZdkBpj8ULbVgKghUxK6Chcf8bdVlwM1V49Bdh5bQyzm4S7r0noePs3Youv1tLblM9aiCT/k1X7OOAE3KohogMVBiGgFhcxI8y3TscIh+eJ9nWLEXC6yuWWRghSNHNC4mm3AJaZAMwwKBgQC3b/yw9U+pxIBbgD1vAPoN7wBmLB7E757w83yez8UeAACy86KGNsddZCBFmGX7kTSfMtOqfoQibZ8bUD2/LWR2XJsJyN9Yz+OobqqMDUNlEzIdeBeLmZmO/xKKdvGTewur2n6SosUeal11OE4dULHshBxyq4SE3GrpIbhl5I5xLwKBgQCMX+g7kKUGky9K6i3p/YrSipBKFgJX2dDFPvcoPNL6vojCEgUbWcrv9stMWS4saxj0NXSyxG3ZIvR274F2JRYDFOMuG5kMhKVwI7Sxo2XdpHMQt8fqguVqcuoaKr/qOJKSR3BV0RTEXIzrg1tTWQ0gAurqHNItVcX0hQDKIaDZGQKBgA8q8F8FebfU6rQmL249S2/ZglPwc/7evWq5B2XuUiM/5s7lPFwSrXt4av62MkCR6iL7xerEGVLrc4uDoAdtzm/HCn7NoEIstmtX8zwvYJI7+xnasz+0zLC5QKQ4X5NNSlLEd1DSqq/wJZgqtrLP2FZxOhT1P7jorOJoCmJKesi7AoGBAJ3yJ16gDnELeGIeKfMs7NT9kCweq87VkXDBgPPS1aklMKvzPcdTTyrBhzstSnM5R8crU33Bdf9t6Uu8DmMIYly8SEg0FaNvoz0BE2/K+Mkod6kMzMNhB8bVzAZCAnoCCiEx96Ny5W9zCOQ7uJiqKemqK1v7N/GPk3fckZOwATUz";


	// 支付宝公钥,查看地址：https://openhome.alipay.com/platform/keyManage.htm 对应APPID下的支付宝公钥。
    public static String alipay_public_key = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAsuLM9TZN796OJIvrkOqS8OT+BPw5xZzCftN7UszlzvO9RnZivy7sinPGz4HyHgjzK8OUKYH3XCxABDWAuUzdaxo8gzusG0rkQcG2FMws11devjUUYLLCRT0vZO//t+GORoiRQMqO6RgLCPjD8nYiYOhejDQSUG5IUkawwjOgnHFtfEbqkT7pj9Q36HI6OBfgycCOI80of0hyav6GLAHmqPOPXtFGU3u6migIWgsA/V7+EOjtwzB6C8kOAHx4cpu9brz+pB41lcCfQjYF/84dgW3su0rdmWxsOrHXVYCPVFIcYWlqA+t15jVgwCwj5n3zcY8tHwplubNTiPBYk3c7DQIDAQAB";

	//
	// 服务器异步通知页面路径  需http://格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问
	public static String notify_url = "http://工程公网访问地址/alipay.trade.page.pay-JAVA-UTF-8/notify_url.jsp";

	// 页面跳转同步通知页面路径 需http://格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问
	public static String return_url = "http://localhost:8080/VideoWebSite/index";

	// 签名方式
	public static String sign_type = "RSA2";
	
	// 字符编码格式
	public static String charset = "utf-8";
	
	// 支付宝网关
	public static String gatewayUrl = "https://openapi.alipaydev.com/gateway.do";
	
	// 支付宝网关
	public static String log_path = "C:\\";


//↑↑↑↑↑↑↑↑↑↑请在这里配置您的基本信息↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑

    /** 
     * 写日志，方便测试（看网站需求，也可以改成把记录存入数据库）
     * @param sWord 要写入日志里的文本内容
     */
//    public static void logResult(String sWord) {
//        FileWriter writer = null;
//        try {
//            writer = new FileWriter(log_path + "alipay_log_" + System.currentTimeMillis()+".txt");
//            writer.write(sWord);
//        } catch (Exception e) {
//            e.printStackTrace();
//        } finally {
//            if (writer != null) {
//                try {
//                    writer.close();
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//            }
//        }
//    }
}

