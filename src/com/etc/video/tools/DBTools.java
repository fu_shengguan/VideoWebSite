package com.etc.video.tools;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.apache.commons.beanutils.BeanUtils;

/**
 * 封装 3.0，后续查询还要优化。
 * 
 * @author admin
 *
 */
public class DBTools {
	

	private static final String URL;
	private static final String USER;
	private static final String PASSWORD;
	private static final String DRIVER;

	static {
		Properties properties = new Properties();
		// 读取配置文件
		InputStream stream = DBTools.class.getResourceAsStream("/db.properties");
		
		try {
			properties.load(stream);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		// 驱动
		DRIVER = properties.getProperty("dbDriver");
		// 用户名
		USER = properties.getProperty("dbUser");
		// 密码
		PASSWORD = properties.getProperty("dbPwd");
		// url
		URL = properties.getProperty("dbURL");
	}

	/**
	 * 获取连接对象的方法
	 * 
	 * @return java.sql.Connection对象
	 */
	public static Connection getConn() {
		Connection connection = null;
		try {
			System.out.println(DRIVER);

			Class.forName(DRIVER);
			connection = DriverManager.getConnection(URL, USER, PASSWORD);
		} catch (SQLException | ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return connection;

	}

	/**
	 * 完成增加删除和修改的方法
	 * 
	 * @param sql    insert update delete语句
	 * @param params 可变数组
	 * @return 受影响的行
	 */
	public static int exUpdate(String sql, Object... params) {
		Connection conn = getConn();
		PreparedStatement pstmt = null;
		int n = 0;
		try {
			pstmt = conn.prepareStatement(sql);
			// ?的个数不确定
			if (params != null) {
				for (int i = 0; i < params.length; i++) {
					pstmt.setObject(i + 1, params[i]);
				}
			}
			// 打印pstmt对象
			System.out.println("pstmt:" + pstmt);
			// 4.执行sql,executeUpdate()返回的是受影响的行
			n = pstmt.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			closeAll(null, pstmt, conn);
		}
		return n;
	}

	/**
	 * 事务管理的方法
	 * 
	 * @param sql    sql语句
	 * @param conn   连接对象
	 * @param params 参数列表
	 * @return 受影响的行
	 * @throws Exception
	 */
	public static int exUpdate(String sql, Connection conn, Object... params) throws Exception {
		PreparedStatement pstmt = null;
		int n = 0;
		try {
			pstmt = conn.prepareStatement(sql);
			// ?的个数不确定
			if (params != null) {
				for (int i = 0; i < params.length; i++) {
					pstmt.setObject(i + 1, params[i]);
				}
			}
			// 打印pstmt对象
			System.out.println("pstmt:" + pstmt);
			// 4.执行sql,executeUpdate()返回的是受影响的行
			n = pstmt.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			throw new RuntimeException("数据库操作异常");
		} finally {
			closeAll(null, pstmt, null);
		}
		return n;
	}

	/**
	 * 通用查询方法,返回值为list;
	 * 
	 * @param sql    查询的sql语句
	 * @param cls    Class类型的对象
	 * @param params 占位符的参数列表
	 * @return Object ->List<Object>
	 */
	public static Object exQuery(String sql, Class cls, Object... params) {

		List<Object> list = new ArrayList<Object>();
		Connection conn = getConn();
		PreparedStatement pstmt = null;
		int n = 0;
		ResultSet rs = null;

		try {
			pstmt = conn.prepareStatement(sql);
			// ?的个数不确定
			if (params != null) {
				for (int i = 0; i < params.length; i++) {
					pstmt.setObject(i + 1, params[i]);
				}
			}
			// 打印pstmt对象
			System.out.println("pstmt:" + pstmt);
			// 4.执行sql,executeQuery()
			rs = pstmt.executeQuery();

			int result = 0;

			// 判断
			if ("java.lang.Object".equals(cls.getName())) {
				if (rs.next()) {
					result = rs.getInt(1);
				}
				closeAll(rs, pstmt, conn);
				return result;
			}

			// 得到的是结果集Result 返回值"可能是" List集合
			// ResultSet-->List
			ResultSetMetaData rsmd = rs.getMetaData();
			// getColumnCount 得到查询的列有几个
			int count = rsmd.getColumnCount();
			// System.out.println("count :" + count);
			// 遍历得到rs数据要使用rs.next
			while (rs.next()) {
				// 根据cls来创建指定类型的对象cls.newInstance(),实际的类型？
				Object bean = cls.newInstance();
				// 达到的目的是将列名和查询的结果依次取出来
				for (int i = 1; i <= count; i++) {
					// 打印输出列名 和列的值
					// System.out.println(rsmd.getColumnLabel(i) + "," + rsmd.getColumnName(i));
					// System.out.println(rsmd.getColumnLabel(i) + "," + rs.getObject(i));
					String name = rsmd.getColumnLabel(i);
					Object value = rs.getObject(i);
					// 急需一个工具，能将列名 + 列值->构造一个对选哪个出来
					// 并给对象进行赋值就可以
					BeanUtils.copyProperty(bean, name, value);
				}
				// 要完成的事情是：构造一个对象出来，并添加到集合

				list.add(bean);
			}

		} catch (SQLException | IllegalAccessException | InvocationTargetException | InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {

			closeAll(rs, pstmt, conn);
		}
		return list;
	}

	/**
	 * 分页的通用方法，mysql数据库 ，基础版
	 * 
	 * @param sql
	 * @param cla
	 * @param page     页码
	 * @param pageSize 每页显示的记录数
	 * @param params
	 * @return
	 */
	public static PageData exQueryByPage(String sql, Class cla, int page, int pageSize, Object... params) {
		// 问题:这里来加limit 还是传递过来之间就加好limit？
		// select .... from ... where ? ... limit ?,?
		// 这里有一个sql 这个sql 查询总的记录数的 给表起别名 as t 别名
		String newsql = "select count(1) from (" + sql + ")  as t";
		// 如果传递的是object对象，查询操作得到就是单个结果
		int totalCount = (int) exQuery(newsql, Object.class, params);

		// 可以加page的判断
		if (page < 1) {
			page = 1;
		}

		// 起始位置的值
		int start = (page - 1) * pageSize;
		// 拼接分页的sql语句
		sql = sql + " limit " + start + "," + pageSize;

		// 页面展示的数据集
		List data = (List) exQuery(sql, cla, params);
		int total = 0;

		// 注意执行这个方法之前确保我们的 totalCount有值
		if (totalCount % pageSize == 0) {  //91%10 ==0
			total = totalCount / pageSize;
		} else {
			total = totalCount / pageSize + 1;
		}
		// 这里给pageData赋值，确保一个页面上的数据是完整的.
		PageData pageData = new PageData<>(data, page, totalCount, pageSize,total);

		return pageData;
	}

	/**
	 * 释放资源
	 * 
	 * @param rs    结果集对象
	 * @param pstmt 预处理对象
	 * @param conn  连接对象
	 */
	public static void closeAll(ResultSet rs, PreparedStatement pstmt, Connection conn) {
		try {
			if (rs != null) {
				rs.close();
			}
			if (pstmt != null) {
				pstmt.close();
			}
			if (conn != null) {
				conn.close();
			}
		} catch (SQLException e) {
			throw new RuntimeException("释放资源异常", e);
		}
	}

	/**
	 * 事务处理操作,稍难
	 * 
	 * @param tran
	 * @return
	 */
	public static Object transaction(ITransaction tran) {
		Connection conn = getConn();
		try {
			conn.setAutoCommit(false);
			Object obj = tran.execute(conn);
			conn.commit();
			return obj;
		} catch (Exception e) {
			try {
				conn.rollback();
			} catch (SQLException e1) {
				throw new RuntimeException("回滚失败!", e1);
			}
			throw new RuntimeException("事务执行失败", e);
		} finally {
			closeAll(null, null, conn);
		}
	}

}
