package com.etc.video.service.impl;

import com.etc.video.dao.impl.UserDaoImpl;
import com.etc.video.dao.UserDao;
import com.etc.video.domain.User;
import com.etc.video.service.UserService;

import java.util.List;

public class UserServiceImpl implements UserService {
    private UserDao userDao = new UserDaoImpl();

    @Override
    public int addUser(User user) {
        return userDao.addUser(user);
    }

    @Override
    public User login(String account) {
        return userDao.login(account);
    }

    @Override
    public int updateInfo(User user) {
        return userDao.updateInfo(user);
    }

    @Override
    public List<User> selectUserByLike(String uname) {
        return userDao.selectUserByLike(uname);
    }

    @Override
    public List<User> getAllUser() {
        return userDao.getAllUser();
    }

    @Override
    public String selectByUid(int uid) {
        return userDao.selectByUid(uid);
    }

    @Override
    public User queryUserByUid(int uid) {
        return userDao.queryUserByUid(uid);
    }
    @Override
    public User selectVip(String account) {
        return userDao.selectVip(account);
    }

    @Override
    public int updateVip(String account) {


        return userDao.updateVip(account);
    }


}
