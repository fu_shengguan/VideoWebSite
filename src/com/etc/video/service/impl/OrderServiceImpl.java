package com.etc.video.service.impl;

import com.etc.video.dao.OrderDao;
import com.etc.video.dao.impl.OrderDaoImpl;
import com.etc.video.domain.Order;
import com.etc.video.service.OrderService;

import java.util.List;

public class OrderServiceImpl implements OrderService {
    private OrderDao orderDao = new OrderDaoImpl();

    @Override
    public int addOrder(Order order) {
        return orderDao.addOrder(order);
    }

    @Override
    public Order selectOrder(String account) {

        return orderDao.selectOrder(account);
    }

    @Override
    public int updateVip(Order order) {
        return orderDao.updateVip(order);
    }

    @Override
    public List<Order> getAllOrder() {
        return orderDao.orderlist();
    }
}
