package com.etc.video.service.impl;

import com.etc.video.dao.VideoDao;
import com.etc.video.dao.impl.VideoDaoImpl;
import com.etc.video.domain.Video;
import com.etc.video.service.VideoService;
import com.etc.video.tools.PageData;

import java.util.List;

public class VideoServiceImpl implements VideoService {
    VideoDao videoDao = new VideoDaoImpl();
    @Override
    public boolean addVideo(Video video) {
      boolean  b1=   videoDao.addVideo(video);
      if(b1){
          System.out.println("service添加视频成功");
      }else{
          System.out.println("service添加视频失败");
      }
        return b1;
    }

    @Override
    public boolean deleVideo(int vdno) {
        return    videoDao.deleVideo(vdno);
    }

    @Override
    public List<Video> queryAllVideo() {


        return videoDao.queryAllVideo();
    }

    @Override
    public boolean updateVideo(int vdno, Video video) {
        return videoDao.updateVideo(vdno,video);
    }



    @Override
    public Video selectByVdno(int vdno) {
        return videoDao.selectByVdno(vdno);
    }

    @Override
    public PageData<Video> selectByUid(String uid, Integer currentpage, Integer pagelength) {

        return    videoDao.selectByUid(uid,currentpage,pagelength);
    }

    @Override
    public PageData<Video> selectByVdname(String vdname,int state, Integer currentpage, Integer pagelength) {
        return  videoDao.selectByVdname(vdname,state,currentpage,pagelength);
    }

    @Override
    public PageData<Video> selectByVdtypeid(int vdtypeid, Integer currentpage, Integer pagelength) {
        return  videoDao.selectByVdtypeid(vdtypeid,currentpage,pagelength);
    }

    @Override
    public boolean updateState(int vdno,int state) {
        return videoDao.updateState(vdno,state);
    }

    @Override
    public List<Video> selectByUidNoPage(int uid) {
        return videoDao.selectByUidNoPage(uid);
    }

    @Override
    public PageData<Video> adminSelectByVdname(String name, int currPage, int pageNum) {
        return videoDao.adminSelectByVdname(name,currPage,pageNum);
    }

}
