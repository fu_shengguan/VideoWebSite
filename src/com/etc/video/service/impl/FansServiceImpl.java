package com.etc.video.service.impl;

import com.etc.video.dao.FansDao;
import com.etc.video.dao.impl.FansDaoImpl;
import com.etc.video.domain.User;
import com.etc.video.service.FansService;
import com.etc.video.tools.PageData;

import java.util.List;
import java.util.Map;

public class FansServiceImpl implements FansService {
    FansDao fansDao  = new FansDaoImpl();
    @Override
    public Boolean addFans(int fansid, int fuid) {

        return     fansDao.addFans(fansid,fuid);
    }

    @Override
    public Boolean deleteFans(int fansid, int fuid) {

        return      fansDao.deleteFans(fansid,fuid);
    }

    @Override
    public int quaryFansCountBuUid(int uid) {
        return fansDao.quaryFansCountBuUid(uid);
    }

    @Override
    public List<Map<String, Object>> selectByUid(int fuid, int currentpage, int pagelength) {
        return fansDao.selectByUid(fuid,currentpage,pagelength);
    }

    @Override
    public PageData<User> selectByFansid(int fansid, int currentpage, int pagelength) {
        return fansDao.selectByFansid(fansid,currentpage,pagelength);
    }

    @Override
    public List<User> queryFansByUid(int uid) {
        return fansDao.queryFansByUid(uid);
    }
    @Override
    public int validateFollow(int fansid, int followid) {
        return fansDao.validateFollow(fansid,followid);
    }

    @Override
    public List<User> queryAuthorByUid(int uid) {
        return fansDao.queryAuthorByUid(uid);
    }
}
