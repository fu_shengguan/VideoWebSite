package com.etc.video.service.impl;

import com.etc.video.dao.VideoTypeDao;
import com.etc.video.dao.impl.VideoTypeDaoImpl;
import com.etc.video.service.VideoTypeService;

public class VideoTypeServiceImpl implements VideoTypeService {
    VideoTypeDao videoTypeDao = new VideoTypeDaoImpl();
    @Override
    public int selectByVideoTypeName(String vdtypename) {
        return videoTypeDao.selectByVideoTypeName(vdtypename);
    }

    @Override
    public String selectByVdTypeId(int vdtypeid) {
        return videoTypeDao.selectByVdTypeId(vdtypeid);
    }
}
