package com.etc.video.service.impl;

import com.etc.video.dao.AdminDao;
import com.etc.video.dao.impl.AdminDaoImpl;
import com.etc.video.domain.Admin;
import com.etc.video.service.AdminSrevice;

import java.util.List;

public class AdminServiceImpl implements AdminSrevice {
    AdminDao adminDao=new AdminDaoImpl();
    @Override
    public int addadmin(Admin admin) {
        return 0;
    }

    @Override
    public boolean deladmin(int admin) {
        return false;
    }

    @Override
    public boolean updateadmin(Admin admin) {
        return false;
    }

    @Override
    public List<Admin> getadmins(String keywords) {
        return adminDao.getadmins(keywords);
    }

    @Override
    public Admin login(String admname) {
        return adminDao.login(admname);
    }

    @Override
    public List<Admin> getadmins() {
        return adminDao.getadmins();
    }
}
