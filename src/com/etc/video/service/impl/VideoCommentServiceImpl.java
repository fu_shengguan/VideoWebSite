package com.etc.video.service.impl;

import com.etc.video.dao.VideoCommentDao;
import com.etc.video.dao.impl.VideoCommentDaoImpl;

import com.etc.video.domain.VideoComment;
import com.etc.video.service.VideoCommentService;

import java.util.List;

public class VideoCommentServiceImpl implements VideoCommentService {
    private VideoCommentDao videoDao=new VideoCommentDaoImpl();

    @Override
    public int addVideoComment(VideoComment videoComment) {
        return videoDao.addVideoComment(videoComment);
    }


    @Override
    public int delVideoComment(VideoComment videoComment) {
        return videoDao.delVideoComment(videoComment);
    }

    @Override
    public List<VideoComment> queryAllComments(int vdno) {
        return videoDao.queryAllComments(vdno);
    }
}
