package com.etc.video.service.impl;

import com.etc.video.dao.VideoFavDao;
import com.etc.video.dao.impl.VideoFavDaoImpl;
import com.etc.video.service.VideoFavService;
import com.etc.video.domain.Video;
import com.etc.video.service.VideoService;

import java.util.List;

public class VideoFavServiceImpl implements VideoFavService {
    VideoFavDao dao = new VideoFavDaoImpl();
    @Override
    public int insert(int uid, int vid) {

        return dao.insert(uid,vid);
    }

    @Override
    public List<Video> showCollectVideo(int uid) {
        return dao.showCollectVideo(uid);
    }

    @Override
    public int delete(int uid, int vid) {
        return dao.delete(uid,vid);
    }

    @Override
    public int validateCollect(int uid, int vid) {

        return dao.validateCollect(uid, vid);
    }

    @Override
    public int countCollectNum(int vid) {
        return dao.countCollectNum(vid);
    }
}
