package com.etc.video.service;

import com.etc.video.domain.Video;

import java.util.List;

public interface VideoFavService {

    /**
     * 插入收藏视频
     * @param uid
     * @param vid
     * @return
     */
    int insert(int uid, int vid);

    /**
     * 展示个人收藏列表
     * @param uid
     * @return
     */
    List<Video> showCollectVideo(int uid);

    /**
     * 用户删除收藏的一个视频
     */
    int delete(int uid, int vid);
    int validateCollect(int uid,int vid);
    int countCollectNum(int vid);
}
