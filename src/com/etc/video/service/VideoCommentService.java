package com.etc.video.service;


import com.etc.video.domain.VideoComment;

import java.util.List;

public interface VideoCommentService {
    /**
     * 增加视频评论
     * @param videoComment
     * @return
     */
    int addVideoComment(VideoComment videoComment);

    /**
     * 删除视频评论
     * @param videoComment
     * @return
     */
    int delVideoComment(VideoComment videoComment);
    /**
     *查询一个视频所有评论
     */
    List<VideoComment> queryAllComments(int vdno);


}
