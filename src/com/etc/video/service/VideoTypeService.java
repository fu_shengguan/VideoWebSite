package com.etc.video.service;

public interface VideoTypeService {
    int selectByVideoTypeName(String vdtypename);//根据视频类型名称查编号

    String selectByVdTypeId(int vdtypeid);//根据视频编号查名称
}
