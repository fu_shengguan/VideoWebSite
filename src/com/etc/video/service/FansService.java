package com.etc.video.service;

import com.etc.video.domain.Fans;
import com.etc.video.domain.User;
import com.etc.video.tools.PageData;

import java.util.List;
import java.util.Map;

public interface FansService {
    Boolean addFans(int fansid, int fuid);//关注作者增加该作者粉丝一列

    Boolean deleteFans(int fansid, int fuid);//取消关注作者删除该作者粉丝一列
    int quaryFansCountBuUid(int uid);//根据作者id查询有多少粉丝
    List<Map<String, Object>> selectByUid(int fuid, int currentpage, int pagelength);//查询有多少粉丝,返回粉丝ID；


    PageData<User> selectByFansid(int fansid, int currentpage, int pagelength);//查询关注多少作者，返回作者ID；

    List<User> queryFansByUid(int uid);

    List<User> queryAuthorByUid(int uid);

    int validateFollow(int fansid,int followid);
}
