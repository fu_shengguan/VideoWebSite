package com.etc.video.service;

import com.etc.video.domain.Admin;

import java.util.List;

public interface AdminSrevice {
    public int addadmin(Admin admin);
    public boolean deladmin(int admin);
    public boolean updateadmin(Admin admin);
    public List<Admin> getadmins(String keywords);
    public Admin login(String admname);
    public List<Admin> getadmins();
}
