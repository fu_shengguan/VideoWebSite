package com.etc.video.service;

import com.etc.video.domain.Order;

import java.util.List;

public interface OrderService {
     int addOrder(Order order);

     Order selectOrder(String account);

     int updateVip(Order order);

     List<Order> getAllOrder();

}
