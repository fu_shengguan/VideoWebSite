package com.etc.video.service;

import com.etc.video.domain.Video;
import com.etc.video.tools.PageData;

import java.util.List;

public interface VideoService {
    /**
     * 上传添加视频
     *
     * @param video
     * @return
     */
    boolean addVideo(Video video);//添加视频

    boolean deleVideo(int vdno);//删除视频

    List<Video> queryAllVideo();//查询所有视频

    boolean updateVideo(int vdno, Video video);//更新视频

    Video selectByVdno(int vdno);//根据视频id查询一个视频

    /**
     * 根据作者id查询多个视频
     *
     * @param uid
     * @return
     */
    PageData<Video> selectByUid(String uid, Integer currentpage, Integer pagelength);

    /**
     * 模糊查询根据视频关键字
     *
     * @param vdname
     * @return
     */
    PageData<Video> selectByVdname(String vdname,int state, Integer currentpage, Integer pagelength);

    /**
     * 根据视频类型id查询多个视频
     * @param vdtypeid
     * @return
     */
    PageData<Video> selectByVdtypeid(int vdtypeid, Integer currentpage, Integer pagelength);

    public boolean updateState(int vdno ,int state);


    List<Video> selectByUidNoPage(int uid);

    PageData<Video>  adminSelectByVdname(String name,int currPage,int pageNum);
}
