package com.etc.video.service;

import com.etc.video.domain.User;

import java.util.List;

public interface UserService {
    int addUser(User user);
    User login(String account);
    int updateInfo(User user);
    public List<User> selectUserByLike(String uname);
    public List<User> getAllUser();

    /*
     *根据用户id查询用户号昵称
     */
    String selectByUid(int uid);

    User queryUserByUid(int uid);
    int updateVip(String account);
    User selectVip(String account);
}
