package com.etc.video.dao;

import com.etc.video.domain.VideoFav;
import com.etc.video.domain.Video;

import java.util.List;

public interface VideoFavDao {

    int insert(int uid, int vid);
    List<Video> showCollectVideo(int uid);
    int delete(int uid, int vid);
    int validateCollect(int uid,int vid);
    int countCollectNum(int vid);
}
