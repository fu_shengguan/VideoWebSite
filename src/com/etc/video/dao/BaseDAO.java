package com.etc.video.dao;

import java.util.List;

public interface BaseDAO<T> {

    /**
     * 添加
     *
     * @param t
     * @return
     */
    int add(T t);

    /**
     * 更新操作
     *
     * @param t
     * @return
     */
    int update(T t);

    /**
     * 删除操作
     *
     * @param id 主键
     * @return
     */
    int del(int id);

    /**
     * 显示所有信息
     *
     * @return
     */
    List<T> queryAll();

    /**
     * 根据主键获取某一个信息
     *
     * @param id
     * @return
     */
    T queryById(int id);

}
