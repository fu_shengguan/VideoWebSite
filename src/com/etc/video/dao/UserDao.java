package com.etc.video.dao;

import com.etc.video.domain.User;

import java.util.List;

public interface UserDao {
    /*
    用户注册
     */
    int addUser(User user);

    /*
    用户登录
     */
    User login(String account);

    /*
    用户更改个人信息
     */
    int updateInfo(User user);

    /*
    用户的查询
     */
    List<User> selectUserByLike(String uname);

    /*
    查询所有用户
     */
    List<User> getAllUser();

    String selectByUid(int uid);

    User queryUserByUid(int uid);

    /*
     查询用户是不是vip
      */
    User selectVip(String account);

    /*
    更新用户的vip状态(添加普通用户为vip)
     */
    int updateVip(String account);
}
