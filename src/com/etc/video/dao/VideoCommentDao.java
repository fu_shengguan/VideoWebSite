package com.etc.video.dao;

import com.etc.video.domain.VideoComment;


import java.util.List;

public interface VideoCommentDao {
    //增加评论
    int addVideoComment(VideoComment videoComment);

    //删除评论
    int delVideoComment(VideoComment videoComment);

    /**
     *查询一个视频所有评论
     */
    List<VideoComment> queryAllComments(int vdno);

}
