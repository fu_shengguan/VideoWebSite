package com.etc.video.dao.impl;

import com.etc.video.dao.FansDao;
import com.etc.video.domain.User;
import com.etc.video.tools.DBTools;
import com.etc.video.tools.PageData;
import com.etc.video.util.DataSourceUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.*;

public class FansDaoImpl implements FansDao {
    int result = -1;
    Connection conn = null;
    PreparedStatement psmt = null;

    @Override
    public Boolean addFans(int fansid, int fuid) {
        String sql = "INSERT INTO tab_fans(fuid,fanid) VALUES(?,?)";
        try {
            //获取Connection对象
            conn = DataSourceUtil.getConnection();
            //            //获取预处理对象 PreparedStatement
            psmt = conn.prepareStatement(sql);
            //如果SQL中有占位符，给占位符赋值，没有过。
            psmt.setInt(1, fuid);
            psmt.setInt(2, fansid);
            //执行SQL: 查询- executeQuery, 增删改- executeUpdate() : int 影响行数;
            result = psmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DataSourceUtil.releaseResource(psmt, conn);
        }
        return result>0;
    }

    /**
     * 取消关注，删除取消关注的作者的粉丝
     *
     * @param fansid 该用户id即为粉丝id
     * @param fuid   该用户关注的作者id
     * @return
     */
    @Override
    public Boolean deleteFans(int fansid, int fuid) {
        String sql1 = "delete from tab_fans where fanid = ? and fuid=?";
        //获取Connection对象
        try {

            conn = DataSourceUtil.getConnection();

            psmt = conn.prepareStatement(sql1);

            psmt.setInt(1, fansid);
            psmt.setInt(2, fuid);
            result = psmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DataSourceUtil.releaseResource(psmt, conn);
        }
        return result>0;
    }


    @Override
    public int quaryFansCountBuUid(int uid) {
        String sq= "select *  from tab_fans where fuid = ?";
        int re =0;
        try {

            conn = DataSourceUtil.getConnection();

            psmt = conn.prepareStatement(sq);

            psmt.setInt(1, uid);

           ResultSet resultSet = psmt.executeQuery();

            while (resultSet.next()) {
                re++;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DataSourceUtil.releaseResource(psmt, conn);
        }
        return re;
    }

    /**
     * 查询关注用户即使作者有多少粉丝关注
     *
     * @param fuid
     * @param currentpage
     * @param pagelength
     * @return
     */
    @Override
    public List<Map<String, Object>> selectByUid(int fuid, int currentpage, int pagelength) {
        String sql = "select uid,account,upwd,uname,phone,email,state,uimg,DATE_FORMAT(birth,'%Y-%m-%d %H:%i:%s'),fancount,available birth from tab_user where uid in(select fanid from  tab_fans where fuid=? )  LIMIT ?,? ";

        List<Map<String, Object>> list = new ArrayList<>();
        User user = new User();
        //获取Connection对象
        try {
            conn = DataSourceUtil.getConnection();
            psmt = conn.prepareStatement(sql);

            psmt.setInt(1, fuid);
            psmt.setInt(2, currentpage);
            psmt.setInt(3, pagelength);
            ResultSet resultSet = psmt.executeQuery();
            while (resultSet.next()) {
                Map<String, Object> map = new HashMap<>();
                map.put("uid",resultSet.getInt("uid"));
                map.put("account",resultSet.getString("account"));
                map.put("upwd",resultSet.getString("upwd"));
                map.put("uname",resultSet.getString("uname"));
                map.put("phone",resultSet.getString("phone"));
                map.put("email",resultSet.getString("email"));
                map.put("state",resultSet.getInt("state"));
                map.put("uimg",resultSet.getString("uimg"));
                map.put("birth",resultSet.getString("birth"));

                map.put("fancount",resultSet.getInt("fancount"));
                map.put("available",resultSet.getInt("available"));
            list.add(map);

            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DataSourceUtil.releaseResource(psmt, conn);
        }

        return list;
    }


    @Override
    public PageData<User> selectByFansid(int fansid, int currentpage, int pagelength) {
String sql = "select uid,account,upwd,uname,phone,email,state,uimg,DATE_FORMAT(birth,'%Y-%m-%d %H:%i:%s') birth,templ from tab_user where uid in(select fanid from  tab_fans where fuid=? ) ";
        PageData<User> pd= DBTools.exQueryByPage(sql, User.class, currentpage,pagelength, fansid);
        return pd;
    }

    @Override
    public List<User> queryFansByUid(int uid) {
        String sql = "SELECT u.* FROM tab_fans f , tab_user u WHERE f.fanid=u.uid AND f.fuid=?";

        return (List<User>) DBTools.exQuery(sql,User.class,uid);
    }

    @Override
    public List<User> queryAuthorByUid(int uid) {
        String sql = "SELECT DISTINCT  u.*  FROM tab_fans f , tab_user u WHERE f.fuid=u.uid AND f.fanid=?";
        return (List<User>) DBTools.exQuery(sql,User.class,uid);
    }
    @Override
    public int validateFollow(int fansid, int followid) {
        String sql = "select * from tab_fans where fanid=? and fuid=?";
        ResultSet rs = null;
        try {
            conn = DataSourceUtil.getConnection();
            psmt = conn.prepareStatement(sql);
            psmt.setInt(1,fansid);
            psmt.setInt(2,followid);
            rs = psmt.executeQuery();
            if(rs.next()){
                result = 1;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            DataSourceUtil.releaseResource(rs,psmt,conn);
        }
        return result;
    }

}
