package com.etc.video.dao.impl;

import com.etc.video.dao.VideoCommentDao;
import com.etc.video.domain.User;
import com.etc.video.domain.Video;
import com.etc.video.domain.VideoComment;
import com.etc.video.tools.DBTools;
import com.etc.video.util.DataSourceUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

public class VideoCommentDaoImpl implements VideoCommentDao {
    @Override
    //增加评论
    public int addVideoComment(VideoComment videoComment) {
        //增加评论SQL语句
        String addSql="INSERT INTO tab_comment(content,cstar,ctime,cuid,vdno) VALUES(?,?,?,?,?)";//,cuid,vdno,?,?
        //受影响行
        int result=-1;
        //数据库连接
        Connection conn = null;
        //预处理
        PreparedStatement psmt = null;
        try {
            //获取connection(数据库连接)对象
            conn= DataSourceUtil.getConnection();
            //获取预处理对象 PreparedStatement
            psmt=conn.prepareStatement(addSql);
            //如果SQL中有占位符，给占位符赋值，没有过
            psmt.setString(1,videoComment.getContent());
            psmt.setInt(2,videoComment.getCstar());
            psmt.setString(3,videoComment.getCtime());
           psmt.setInt(4,videoComment.getCuid());
            psmt.setInt(5,videoComment.getVdno());


            //受影响行数
            result = psmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            DataSourceUtil.releaseResource(psmt , conn);
        }
        return result;
    }

    @Override
    //删除评论
    public int delVideoComment(VideoComment videoComment) {
        String delSql="DELETE FROM `tab_comment` WHERE cid=?";
        int result=-1;
        Connection conn= null;
        PreparedStatement psmt= null;
        try {
            //获取数据库连接(connection)对象
            conn= DataSourceUtil.getConnection();
            //获取预处理对象 PreparedStatement
            psmt=conn.prepareStatement(delSql);
            //如果SQL中有占位符，给占位符赋值，没有过
            psmt.setInt(1,videoComment.getCid());
            //受影响行数
            result = psmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
          DataSourceUtil.releaseResource(psmt , conn);
        }
        return result;
    }

    @Override
    public List<VideoComment> queryAllComments(int vdno) {

        String sql = "select * from tab_comment where vdno=? ORDER BY cid desc";


        return  (List<VideoComment>) DBTools.exQuery(sql,VideoComment.class,vdno);
    }
}

