package com.etc.video.dao.impl;

import com.etc.video.dao.AdminDao;
import com.etc.video.domain.Admin;
import com.etc.video.tools.DBTools;

import java.sql.ResultSet;
import java.util.List;

public class AdminDaoImpl implements AdminDao {
    @Override
    public int addadmin(Admin admin) {
    return 0;

    }

    @Override
    public boolean deladmin(int admin) {
        return false;
    }

    @Override
    public boolean updateadmin(Admin admin) {
        return false;
    }

    @Override
    public List<Admin> getadmins(String keywords) {
        String sql = "select * from tab_admin where admname like ?";
        List<Admin> list= (List<Admin>) DBTools.exQuery(sql,Admin.class,"%" + keywords + "%");
        return list;
    }

    @Override
    public List<Admin> getadmins() {
        String sql = "select * from tab_admin";
        List<Admin> list= (List<Admin>) DBTools.exQuery(sql,Admin.class);
        return list;
    }

    @Override
    public Admin login(String admname) {
        String sql="select * from tab_admin where admname = ?";
        List<Admin> list = (List<Admin>) DBTools.exQuery(sql, Admin.class, admname);
        if (list.size() == 0) {
            return null;
        }
        return list.get(0);
    }
}
