package com.etc.video.dao.impl;

import com.etc.video.dao.OrderDao;
import com.etc.video.domain.Order;
import com.etc.video.util.DataSourceUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class OrderDaoImpl implements OrderDao {


    @Override
    public int addOrder(Order order) {
        String sql = "INSERT INTO tab_order(orderno,ordername,orderprice,orderstart,ordertime,orderend,account) VALUES (?,?,?,?,?,?,?)";
        Connection conn = null;
        PreparedStatement psmt = null;
        int result = -1;

        try {
            conn = DataSourceUtil.getConnection();
            psmt = conn.prepareStatement(sql);
            psmt.setString(1, order.getOrderno());
            psmt.setString(2, order.getOrdername());
            psmt.setDouble(3, order.getOrderprice());
            psmt.setString(4, order.getOrderstart());
            psmt.setInt(5, order.getOrdertime());
            psmt.setString(6, order.getOrderend());
            psmt.setString(7, order.getAccount());

            result = psmt.executeUpdate();


        } catch (SQLException e) {
            e.printStackTrace();
        }

        return result;
    }

    /*
    根据用户查找id
     */
    @Override
    public Order selectOrder(String account) {
        String sql = "select * from tab_order where account = ?";
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet rs = null;
        Order order = null;

        try {
            connection = DataSourceUtil.getConnection();
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1,account);

            rs = preparedStatement.executeQuery();



            while(rs.next()) {
                order = new Order();
                order.setOrderno(rs.getString(2));
                order.setOrdername(rs.getString(3));
                order.setOrderprice(rs.getDouble(4));
                order.setOrderstart(rs.getString(5));
                order.setOrdertime(rs.getInt(6));
                order.setOrderend(rs.getString(7));


            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return order;
    }

    @Override
    public List<Order> orderlist() {
        String sql = "select * from tab_order";
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet rs = null;
        Order order = null;
        List<Order> orderlist = new ArrayList<>();
        try {
            connection = DataSourceUtil.getConnection();
            preparedStatement = connection.prepareStatement(sql);

            rs = preparedStatement.executeQuery();

            while (rs.next()) {
                order = new Order();
                order.setOrderno(rs.getString(2));
                order.setOrdername(rs.getString(3));
                order.setOrderprice(rs.getDouble(4));
                order.setOrderstart(rs.getString(5));
                order.setOrdertime(rs.getInt(6));
                order.setOrderend(rs.getString(7));
                order.setAccount(rs.getString(8));
                orderlist.add(order);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }


        return orderlist;
    }

    @Override
    public int updateVip(Order order) {
        String sql = "update tab_order set orderstart = ?,ordertime = ?,orderend =? where account = ?";
        int result = -1;
        Connection conn = null;
        PreparedStatement psmt = null;
        try {
            conn = DataSourceUtil.getConnection();
            psmt = conn.prepareStatement(sql);
            psmt.setString(1, order.getOrderstart());
            psmt.setInt(2, order.getOrdertime());
            psmt.setString(3, order.getOrderend());
            psmt.setString(4, order.getAccount());
            result = psmt.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return result;
    }
}
