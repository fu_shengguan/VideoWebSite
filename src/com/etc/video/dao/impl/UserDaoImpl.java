package com.etc.video.dao.impl;

import com.etc.video.dao.UserDao;
import com.etc.video.domain.User;
import com.etc.video.tools.DBTools;
import com.etc.video.util.DataSourceUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


public class UserDaoImpl implements UserDao {

    @Override
    public int addUser(User user) {
        String sql = "INSERT INTO tab_user(account,upwd) VALUES (?,?)";
        int result = -1;
        Connection conn = null;
        PreparedStatement psmt = null;
        try {

            //获取数据库连接对象
            conn = DataSourceUtil.getConnection();
            //获取预处理对象 preparestatement
            psmt = conn.prepareStatement(sql);
            //sql中的占位符进行赋值
            psmt.setString(1, user.getAccount());
            psmt.setString(2, user.getUpwd());
//
            result = psmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return result;
    }

    @Override
    public User login(String account) {

        //这里先不用实例化,等到rs.next可以执行下去在进行实例化
        User user = null;
        String sql = "select * from tab_user where account = ?";
        Connection conn = null;
        ResultSet rs = null;
        PreparedStatement psmt = null;
        try {
            conn = DataSourceUtil.getConnection();
            psmt = conn.prepareStatement(sql);
            psmt.setString(1, account);
            rs = psmt.executeQuery();
            if (rs.next()) {
                user = new User();
                user.setUid(rs.getInt(1));
                user.setAccount(rs.getString(2));
                user.setUpwd(rs.getString(3));
                user.setUname(rs.getString(4));
                user.setPhone(rs.getString(5));
                user.setEmail(rs.getString(6));
                user.setState(rs.getInt(7));
                user.setUimg(rs.getString(8));
                user.setBirth(rs.getString(9));
                user.setFancount(rs.getInt(10));
                user.setAvailable(rs.getInt(11));


            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DataSourceUtil.releaseResource(rs, psmt, conn);
        }
        return user;
    }

    @Override
    public int updateInfo(User user) {
        String sql = "UPDATE tab_user SET uname = ?,phone  = ?,email =?,birth = ? WHERE account = ?;";
        Connection conn = null;
        PreparedStatement psmt = null;
        int result = -1;

        try {
            conn = DataSourceUtil.getConnection();
            psmt = conn.prepareStatement(sql);
            psmt.setString(1, user.getUname());
            psmt.setString(2, user.getPhone());
            psmt.setString(3, user.getEmail());

            psmt.setString(4, user.getBirth());
            psmt.setString(5, user.getAccount());

            result = psmt.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }


        return result;
    }

    @Override
    public List<User> selectUserByLike(String uname) {
        List<User> list = new ArrayList<>();
        //这里先不用实例化,等到rs.next可以执行下去在进行实例化
        User user = null;
        String sql = "select * from tab_user where 1=1 ";
        if (uname != null && !"".equals(uname.trim())) {
            sql += " and uname like concat('%',?,'%')";
        }

        Connection conn = null;
        ResultSet rs = null;
        PreparedStatement psmt = null;
        try {
            conn = DataSourceUtil.getConnection();
            psmt = conn.prepareStatement(sql);
            if (uname != null && !"".equals(uname.trim())) {

                //psmt.setString(1, "%"+uname+"%");
                psmt.setString(1, uname);

            }

            rs = psmt.executeQuery();
            while (rs.next()) {
                user = new User();
                user.setUid(rs.getInt(1));
                user.setAccount(rs.getString(2));
                user.setUpwd(rs.getString(3));
                user.setUname(rs.getString(4));
                user.setPhone(rs.getString(5));
                user.setEmail(rs.getString(6));
                user.setState(rs.getInt(7));
                user.setUimg(rs.getString(8));
                user.setBirth(rs.getString(9));
                user.setFancount(rs.getInt(10));
                user.setAvailable(rs.getInt(11));
                list.add(user);

            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DataSourceUtil.releaseResource(rs, psmt, conn);
        }
        return list;
    }

    @Override
    public List<User> getAllUser() {
        List<User> list = new ArrayList<>();
        //这里先不用实例化,等到rs.next可以执行下去在进行实例化
        User user = null;
        String sql = "select * from tab_user  ";


        Connection conn = null;
        ResultSet rs = null;
        PreparedStatement psmt = null;
        try {
            conn = DataSourceUtil.getConnection();
            psmt = conn.prepareStatement(sql);

            rs = psmt.executeQuery();
            while (rs.next()) {
                user = new User();
                user.setUid(rs.getInt(1));
                user.setAccount(rs.getString(2));
                user.setUpwd(rs.getString(3));
                user.setUname(rs.getString(4));
                user.setPhone(rs.getString(5));
                user.setEmail(rs.getString(6));
                user.setState(rs.getInt(7));
                user.setUimg(rs.getString(8));
                user.setBirth(rs.getString(9));
                user.setFancount(rs.getInt(10));
                user.setAvailable(rs.getInt(11));
                list.add(user);

            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DataSourceUtil.releaseResource(rs, psmt, conn);
        }
        return list;
    }

    @Override
    public String selectByUid(int uid) {
        String  sql = "select uname from tab_user where uid=? ";
        List<User> list =  (List<User>)  DBTools.exQuery(sql,User.class,uid);
        return list.get(0).getUname();
    }

    @Override
    public User queryUserByUid(int uid) {
        String sql="select * from  tab_user where uid=?";
         List<User> list = (List<User>) DBTools.exQuery(sql,User.class,uid);
        return list.get(0);
    }

    @Override
    public User selectVip(String account) {
        String sql = "select * from tab_user where account = ?";
        Connection conn = null;
        PreparedStatement psmt = null;
        ResultSet rs =null;
        User user=null;
        int result = 0;
        try {
            conn= DataSourceUtil.getConnection();
            psmt = conn.prepareStatement(sql);
            psmt.setString(1,account);
            rs=psmt.executeQuery();
            if (rs.next()){
                user = new User();
                user.setUid(rs.getInt(1));
                user.setAccount(rs.getString(2));
                user.setUpwd(rs.getString(3));
                user.setUname(rs.getString(4));
                user.setPhone(rs.getString(5));
                user.setEmail(rs.getString(6));
                user.setState(rs.getInt(7));
                user.setUimg(rs.getString(8));
                user.setBirth(rs.getString(9));
                user.setFancount(rs.getInt(10));
                user.setAvailable(rs.getInt(11));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return user;
    }

    @Override
    public int updateVip(String account) {
        String sql = "update tab_user set state = 2 where account =?";
        Connection conn = null;
        PreparedStatement psmt = null;
        int result = 0;
        try {
            conn=DataSourceUtil.getConnection();
            psmt = conn.prepareStatement(sql);

            psmt.setString(1, account);
            result  = psmt.executeUpdate();


        } catch (SQLException e) {
            e.printStackTrace();
        }


        return result;
    }



}
