package com.etc.video.dao.impl;

import com.etc.video.dao.VideoTypeDao;
import com.etc.video.domain.Video;
import com.etc.video.domain.VideoType;
import com.etc.video.util.DataSourceUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class VideoTypeDaoImpl implements VideoTypeDao {
    int result = -1;
    Connection conn = null;
    PreparedStatement psmt = null;
    @Override
    public int selectByVideoTypeName(String vdtypename) {
        String sql = "select * from  tab_video_type where vdtypename=?";
        VideoType videoType = new VideoType();
        //获取Connection对象
        try {
            conn = DataSourceUtil.getConnection();
            psmt = conn.prepareStatement(sql);

            psmt.setString(1, vdtypename);
            ResultSet resultSet = psmt.executeQuery();
            while (resultSet.next()) {
                videoType.setVdtypename(vdtypename);
                videoType.setVdtypeid(resultSet.getInt("vdtypeid"));
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DataSourceUtil.releaseResource(psmt, conn);
        }

        return videoType.getVdtypeid();
    }

    @Override
    public String selectByVdTypeId(int vdtypeid) {
        String sql = "select vdtypename from  tab_video_type where vdtypeid=?";
        VideoType videoType = new VideoType();
        //获取Connection对象
        try {
            conn = DataSourceUtil.getConnection();
            psmt = conn.prepareStatement(sql);

            psmt.setInt(1, vdtypeid);
            ResultSet resultSet = psmt.executeQuery();
            while (resultSet.next()) {
                videoType.setVdtypeid(vdtypeid);
                videoType.setVdtypename(resultSet.getString("vdtypename"));
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DataSourceUtil.releaseResource(psmt, conn);
        }

        return videoType.getVdtypename();
    }
}
