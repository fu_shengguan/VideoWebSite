package com.etc.video.dao.impl;

import com.etc.video.dao.VideoFavDao;
import com.etc.video.domain.Video;
import com.etc.video.domain.VideoComment;
import com.etc.video.domain.VideoFav;
import com.etc.video.util.DataSourceUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class VideoFavDaoImpl implements VideoFavDao {


    @Override
    public int insert(int uid, int vid) {
        Connection conn = null;
        PreparedStatement psmt = null;
        int result = -1;
        String sql = "insert into tab_fav(uid,vdno) values(?,?) ";
        try {
            conn = DataSourceUtil.getConnection();
            psmt = conn.prepareStatement(sql);
            psmt.setInt(1, uid);
            psmt.setInt(2, vid);
            result = psmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DataSourceUtil.releaseResource(psmt, conn);
        }
        return result;
    }

    @Override
    public List<Video> showCollectVideo(int uid) {
        Connection conn = null;
        PreparedStatement psmt = null;
        ResultSet rs = null;
        List<Video> list = new ArrayList<>();
        Video v = null;
        String sql = "select v.vdname,v.videotime,v.vdno,v.vdtypeid," +
                "v.summary,v.cover,v.uid,v.updatetime,v.vip,v.state,v.address" +
                " from tab_video v,tab_fav f where v.vdno=f.vdno and f.uid=?";

        try {
            conn = DataSourceUtil.getConnection();
            psmt = conn.prepareStatement(sql);
            psmt.setInt(1, uid);
            rs = psmt.executeQuery();
            while (rs.next()) {
                v = new Video();
                v.setVdname(rs.getString(1));
                v.setVideotime(rs.getString(2));
                v.setVdno(rs.getInt(3));
                v.setVdtypeid(rs.getInt(4));
                v.setSummary(rs.getString(5));
                v.setCover(rs.getString(6));
                v.setUid(rs.getInt(7));
                v.setUpdatetime(rs.getString(8));
                v.setVip(rs.getInt(9));
                v.setState(rs.getInt(10));
                v.setAddress(rs.getString(11));
                list.add(v);
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            DataSourceUtil.releaseResource(rs, psmt, conn);
        }
        return list;
    }

    @Override
    public int delete(int uid, int vid) {
        Connection conn = null;
        PreparedStatement psmt = null;
        int result = -1;
        String sql = "delete from tab_fav where uid=? and vdno=?";
        try {
            conn = DataSourceUtil.getConnection();
            psmt = conn.prepareStatement(sql);
            psmt.setInt(1, uid);
            psmt.setInt(2, vid);
            result = psmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DataSourceUtil.releaseResource(psmt, conn);
        }
        return result;
    }

    @Override
    public int validateCollect(int uid, int vid) {
        Connection conn = null;
        PreparedStatement psmt = null;
        ResultSet rs = null;
        int result = -1;
        String sql = "select * from tab_fav  where vdno=? and uid=?";
        try {

            conn = DataSourceUtil.getConnection();
            psmt = conn.prepareStatement(sql);
            psmt.setInt(1, vid);
            psmt.setInt(2, uid);
            rs = psmt.executeQuery();
            if (rs.next()) {
                result = 1;
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            DataSourceUtil.releaseResource(rs, psmt, conn);
        }
        return result;
    }

    @Override
    public int countCollectNum(int vid) {
        Connection conn = null;
        PreparedStatement psmt = null;
        ResultSet rs = null;
        int count = 0;
        String sql = "SELECT * FROM tab_fav f WHERE f.`vdno`=?";
        try {
            conn = DataSourceUtil.getConnection();
            psmt = conn.prepareStatement(sql);
            psmt.setInt(1, vid);
            rs = psmt.executeQuery();
            while(rs.next()){
                count++;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            DataSourceUtil.releaseResource(rs,psmt,conn);
        }


        return count;
    }
}
