package com.etc.video.dao.impl;

import com.etc.video.dao.VideoDao;
import com.etc.video.domain.Video;
import com.etc.video.tools.DBTools;
import com.etc.video.tools.PageData;
import com.etc.video.util.DataSourceUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class VideoDaoImpl implements VideoDao {
    int result = -1;
    Connection conn = null;
    PreparedStatement psmt = null;


    @Override
    public Boolean addVideo(Video video) {
        if (video==null) {
            return false;
        }
        String sql = "INSERT INTO tab_video(vdname,vdtypeid,summary,cover,uid,updatetime,vip,state,address,videotime) VALUES(?,?,?,?,?,now(),?,?,?,?)";
        return DBTools.exUpdate(sql, video.getVdname(),video.getVdtypeid(),video.getSummary(),video.getCover(),video.getUid(),video.getVip(),video.getState(),video.getAddress(),video.getVideotime()) > 0;
    }

    @Override
    public Boolean deleVideo(int vdno) {

        String sql1 = "delete from tab_video where vdno = ?";
        //获取Connection对象
        try {

            conn = DataSourceUtil.getConnection();
            //获取预处理对象 PreparedStatement
            psmt = conn.prepareStatement(sql1);
            //如果SQL中有占位符，给占位符赋值，没有过。
            psmt.setInt(1, vdno);
            result = psmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DataSourceUtil.releaseResource(psmt, conn);
        }

        return result>0;
    }

    @Override
    public Boolean updateVideo(int vdno, Video video) {
        String sql = "update tab_video set vdname=?,vdtypeid=?,summary=?,cover=?,uid=?,updatetime=?,vip=?,state=?,videotime=? where vdno=?";
        //获取Connection对象
        try {
            //获取预处理对象 PreparedStatement
            psmt = conn.prepareStatement(sql);
            //如果SQL中有占位符，给占位符赋值，没有过。
            psmt.setString(1, video.getVdname());
            psmt.setInt(2, video.getVdtypeid());
            psmt.setString(3, video.getSummary());
            psmt.setString(4, video.getCover());
            psmt.setInt(5, video.getUid());
            psmt.setString(6, video.getUpdatetime());
            psmt.setInt(7, video.getVip());
            psmt.setInt(8, video.getState());
            psmt.setString(9, video.getVideotime());
            psmt.setInt(10, vdno);
            result = psmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DataSourceUtil.releaseResource(psmt, conn);
        }

        return result>0;
    }

    @Override
    public Video selectByVdno(int vdno) {
        String sql = "select * from  tab_video where vdno=?";
        Video video = new Video();
        //获取Connection对象
        try {
            conn = DataSourceUtil.getConnection();
            psmt = conn.prepareStatement(sql);

            psmt.setInt(1, vdno);
            ResultSet resultSet = psmt.executeQuery();
            while (resultSet.next()) {
                video.setVdno(resultSet.getInt("vdno"));
                video.setVdname(resultSet.getString("vdname"));
                video.setVdtypeid(resultSet.getInt("vdtypeid"));
                video.setSummary(resultSet.getString("summary"));
                video.setCover(resultSet.getString("cover"));
                video.setUid(resultSet.getInt("uid"));
                video.setUpdatetime(resultSet.getString("updatetime"));
                video.setVip(resultSet.getInt("vip"));
                video.setState(resultSet.getInt("state"));
                video.setAddress(resultSet.getString("address"));
                video.setVideotime(resultSet.getString("videotime"));
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DataSourceUtil.releaseResource(psmt, conn);
        }

        return video;
    }

    @Override
    public List<Video> queryAllVideo() {
        String sql = "select * from tab_video  ORDER BY vdno ASC";


        return  (List<Video>) DBTools.exQuery(sql,Video.class);
    }

    @Override
    public PageData<Video> selectByUid(String uid, Integer currentpage, Integer pagelength) {
        String sql = "select * from  tab_video where uid=? ";
        PageData<Video> pageData = DBTools.exQueryByPage(sql,Video.class,currentpage,pagelength,uid);
        return pageData;
    }
    @Override
    public PageData<Video> selectByVdname(String vdname, int state,Integer currentpage, Integer pagelength) {
        String sql = "SELECT * FROM tab_video where state=1";
        if (vdname != null && !"".equals(vdname.trim())) {
            sql += " and vdname like concat('%',?,'%')";
            PageData<Video> pageData1 = DBTools.exQueryByPage(sql,Video.class,currentpage,pagelength, vdname);
            return pageData1;
        }
        PageData<Video> pageData1 = DBTools.exQueryByPage(sql,Video.class,currentpage,pagelength);
        return pageData1;
    }
    @Override
    public PageData<Video> selectByVdtypeid(int vdtypeid, Integer currentpage, Integer pagelength) {
        String sql = "select * from tab_video  WHERE vdtypeid=?";
        PageData<Video> pageData = DBTools.exQueryByPage(sql,Video.class,currentpage,pagelength,vdtypeid);
        return pageData;
    }
    @Override
    public boolean updateState(int vdno,int state) {
        String sql="update tab_video set state = ? where vdno = ?";
        int aa =DBTools.exUpdate(sql,state,vdno);
        if (aa>0){
            return true;
        }else {
            return false;
        }

    }

    @Override
    public List<Video> selectByUidNoPage(int uid) {
        String sql = "select * from  tab_video where uid=? ";
        return (List<Video>) DBTools.exQuery(sql,Video.class,uid);
    }

    @Override
    public PageData<Video> adminSelectByVdname(String vdname, int currentpage, int pagelength) {
        String sql = "SELECT * FROM tab_video where 1=1";
        if (vdname != null && !"".equals(vdname.trim())) {
            sql += " and vdname like concat('%',?,'%')";
            PageData<Video> pageData1 = DBTools.exQueryByPage(sql,Video.class,currentpage,pagelength, vdname);
            return pageData1;
        }
        PageData<Video> pageData1 = DBTools.exQueryByPage(sql,Video.class,currentpage,pagelength);
        return pageData1;
    }

}
