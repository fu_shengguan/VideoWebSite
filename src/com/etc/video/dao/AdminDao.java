package com.etc.video.dao;

import com.etc.video.domain.Admin;

import java.util.List;

public interface AdminDao {
   public int addadmin(Admin admin);
    public boolean deladmin(int admid);
    public boolean updateadmin(Admin admin);
    public List<Admin> getadmins(String keywords);
    public List<Admin> getadmins();
    public Admin login(String admname);
}
