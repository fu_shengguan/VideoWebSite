package com.etc.video.dao;

import com.etc.video.domain.Order;

import java.util.List;

public interface OrderDao {
    /*
    添加订单
     */
    int addOrder(Order order);

    /*
    查询订单
     */
    Order selectOrder(String account);

    /*
    查询所有订单
     */
    List<Order> orderlist();

    /*
    用户续费
     */
    int updateVip(Order order);

}
