package com.etc.video.filter;

import com.etc.video.domain.User;
import com.etc.video.domain.Video;
import com.etc.video.service.VideoService;
import com.etc.video.service.impl.VideoServiceImpl;
import com.etc.video.util.UrlUtil;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebFilter("/single.html")
public class LoginFilter implements Filter {
    public void destroy() {
    }

    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws ServletException, IOException {
        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) resp;

        String uri = request.getRequestURI();
        if (uri.endsWith("/userlogin.html") || uri.contains("/css") || uri.contains("/js") || uri.endsWith("/userRegister.html") || uri.endsWith("/login.do")
                || uri.endsWith("/userlogin.do")) {
            chain.doFilter(request, response);
        } else {
            HttpSession session = request.getSession();
            if (session.getAttribute("user") == null) {
                response.sendRedirect("userlogin.html");
            } else {
                User user = (User) session.getAttribute("user");
                String url = String.valueOf(request.getRequestURL());
          /*      UrlUtil.UrlEntity entity =  UrlUtil.parse(url);
                String vdno  =  entity.params.get("vdno");
            */

                String queryString = request.getQueryString();
                System.out.println(queryString);
                String[] a = queryString.split("=");
                VideoService videoService = new VideoServiceImpl();
                Video video = videoService.selectByVdno(Integer.valueOf(a[1]));
                System.out.println("视频编号:"+video.getVdno()+"   用户会员状态"+user.getState()+user.getUname());

                if (video.getVip() == 1) {
                    if(user.getState()==2){
                        chain.doFilter(request, response);
                    }else{
                        System.out.println("视频编号:"+video.getVdno()+"   用户会员状态"+user.getState()+user.getUname());
                        response.sendRedirect("index.html?sorry=0");
                    }
                } else {
                    chain.doFilter(request, response);
                }

            }
        }

    }

    public void init(FilterConfig config) throws ServletException {

    }

}
