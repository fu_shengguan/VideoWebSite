package com.etc.video.listener;

import javax.servlet.ServletContext;
import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

/**
 * Application Lifecycle Listener implementation class TestSessionListener
 *
 */
//@WebListener
public class TestSessionListener implements HttpSessionListener {

//	private int c=0;
    /**
     * Default constructor. 
     */
    public TestSessionListener() {
        // TODO Auto-generated constructor stub
    	System.out.println("TestSessionListener 构造方法  显示被监听。。。。");
    }

	/**
	 * sessionCreated  Session被创建 监听的就是Session被创建事件
     * @see HttpSessionListener#sessionCreated(HttpSessionEvent)
     */
    public void sessionCreated(HttpSessionEvent se)  { 
         // TODO Auto-generated method stub
//    	se.getSession().setAttribute("c", ++c);
    	//要将数据存在jsp》appliction》 Servlet》SercletContext
    	ServletContext application=se.getSession().getServletContext();
    	//先判断在application是否存在属性
    	if (application.getAttribute("count")==null) {
			//在首次后从来没有人访问过，设置count的值为1
    		application.setAttribute("count", 1);
		}else {
			//之前有人访问过，这里需要转型，需要进行数学计算
			Integer obj=(Integer) application.getAttribute("count");
			//将count的值为原来的值加一
			application.setAttribute("count", obj+1);
		}
    	System.out.println("TestSessionListener sessionCreated方法 开启新窗口。。。。");
    }

	/**
	 * sessionDestroyed被销毁的方法，，监听的就是Session被销毁
     * @see HttpSessionListener#sessionDestroyed(HttpSessionEvent)
     */
    public void sessionDestroyed(HttpSessionEvent se)  { 
         // TODO Auto-generated method stub
//    	se.getSession().setAttribute("c", --c);
    	ServletContext application=se.getSession().getServletContext();
    	//先判断在application是否存在属性
    	if (application.getAttribute("count")!=null) {
			//在首次后从来没有人访问过，设置count的值为1
    		application.setAttribute("count", 1);
		}else {
			//之前有人访问过，这里需要转型，需要进行数学计算
			Integer obj=(Integer) application.getAttribute("count");
			//将count的值为原来的值减一
			application.setAttribute("count", obj-1);
		}
    	System.out.println("TestSessionListener sessionDestroyed方法销毁。。。。");
    }
	
}
