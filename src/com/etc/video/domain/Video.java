package com.etc.video.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Video {
    private int vdno;//视频编号
    private String vdname;//视频视频名称
    private int vdtypeid;//视频类别编号
    private String summary;//视频简介
    private String cover;//视频封面地址
    private int uid;//上传者id
    private String updatetime;//上传时间
    private int vip;//0:普通用户可看 1:会员可看
    private int state;//0:待审核,1:审核通过 2:审核未通过
    private String address;//视频地址
    private String videotime;
    public Video(int vdno, int state) {
        this.vdno = vdno;
        this.state = state;
    }
    @Override
    public String toString() {
        return "Video{" +
                "vdno=" + vdno +
                ", vdname='" + vdname + '\'' +
                ", vdtypeid=" + vdtypeid +
                ", summary='" + summary + '\'' +
                ", cover='" + cover + '\'' +
                ", uid=" + uid +
                ", updatetime='" + updatetime + '\'' +
                ", vip=" + vip +
                ", state=" + state +
                ", address='" + address + '\'' +
                ", videotime='" + videotime + '\'' +
                '}';
    }
}
