package com.etc.video.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class User implements Serializable {
    private int uid;
    private String account;
    private String upwd;
    private String uname;
    private String phone;
    private String email;
    private int state;
    private String uimg;
    private String birth;
    private String temp1;
    private int fancount;
    private int available;


    public User(String account, String upwd, String uname, String phone, String email, String uimg, String birth) {
        this.account = account;
        this.upwd = upwd;
        this.uname = uname;
        this.phone = phone;
        this.email = email;
        this.uimg = uimg;
        this.birth = birth;
    }

    public User(String account, String upwd) {
        this.account = account;
        this.upwd = upwd;
    }


}
