package com.etc.video.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Fans {
     int fid;//关注编号
     int fuid;//作者ID
    int fanid;//粉丝ID
}
