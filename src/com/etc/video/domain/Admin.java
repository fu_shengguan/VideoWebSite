package com.etc.video.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;


public class Admin implements Serializable {
    private int admid;
    private String admname;
    private String admpwd;

    @Override
    public String toString() {
        return "Admin{" +
                "admid=" + admid +
                ", admname='" + admname + '\'' +
                ", admpwd='" + admpwd + '\'' +
                '}';
    }

    public Admin(int admid, String admname, String admpwd) {
        this.admid = admid;
        this.admname = admname;
        this.admpwd = admpwd;
    }

    public Admin() {
    }

    public int getAdmid() {
        return admid;
    }

    public void setAdmid(int admid) {
        this.admid = admid;
    }

    public String getAdmname() {
        return admname;
    }

    public void setAdmname(String admname) {
        this.admname = admname;
    }

    public String getAdmpwd() {
        return admpwd;
    }

    public void setAdmpwd(String admpwd) {
        this.admpwd = admpwd;
    }
}
