package com.etc.video.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class VideoLocal {
    private int vdno;//视频编号
    private String vdname;//视频视频名称
    private String vdtypename;//视频类别名称
    private String summary;//视频简介
    private String cover;//视频封面地址
    private String uname;//上传者昵称
    private String updatetime;//上传时间
    private String vip;//0:普通用户可看 1:会员可看
    private String state;//0:待审核,1:审核通过 2:审核未通过
    private String address;//视频地址
    private String videotime;
    private int fans;


}
