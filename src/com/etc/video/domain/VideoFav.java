package com.etc.video.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
@Data
@AllArgsConstructor
@NoArgsConstructor
public class VideoFav implements Serializable {
    private int no;
    private int       uid;
    private  int      vdno;
    private   String     collectime;

    public VideoFav(int uid, int vdno) {
        this.uid = uid;
        this.vdno = vdno;
    }
}
