package com.etc.video.domain;

import lombok.*;

@Data//包含get,set，toString方法
@NoArgsConstructor//无参构造
@AllArgsConstructor//有参构造  全参
//评论区  属性
public class VideoComment {

    //评论编号
    private int cid;

    //评论内容
    private String content;

    //发布该评论的时间
    private String ctime;

    //评论该视频用户的id
    private int cuid;

    //星级
    private int cstar;

    //被评论视频的编号
    private int vdno;

    //添加评论 有参构造 评论内容 星级 评论时间 用户 视频编号
    public VideoComment(String content,int cstar,String ctime,int cuid,int vdno) {//,int cuid,int vdno
        this.content = content;
        this.cstar = cstar;
        this.ctime=ctime;
        this.cuid=cuid;
        this.vdno=vdno;
    }

//删除评论
    public VideoComment(int cid) {
        this.cid = cid;}
}
