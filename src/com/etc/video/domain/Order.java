package com.etc.video.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Order {
   private int orderid;
   private String orderno;
   private String ordername;
   private double orderprice;
   private String orderstart;
   private int ordertime;
   private String orderend;
   private String  account;
}
