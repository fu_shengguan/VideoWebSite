package com.etc.video.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class VideoType {
    private  int vdtypeid;//视频类型编号
    private String vdtypename;//视频类型名称

}
