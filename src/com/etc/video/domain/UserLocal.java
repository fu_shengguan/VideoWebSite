package com.etc.video.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserLocal {
    private int uid;
    private String account;
    private String upwd;
    private String uname;
    private String phone;
    private String email;
    private int state;
    private String uimg;
    private String birth;
    private String temp1;
    private int fancount;
    private int available;
}
