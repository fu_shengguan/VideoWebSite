package com.etc.video.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class VideoCommentLocal {
    //评论编号
    private int cid;

    //评论内容
    private String content;

    //发布该评论的时间
    private String ctime;

    //评论该视频用户的昵称
    private String cuname;

    //星级
    private int cstar;

    //被评论视频的编号
    private int vdno;

}
