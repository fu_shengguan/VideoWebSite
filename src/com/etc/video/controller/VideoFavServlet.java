package com.etc.video.controller;

import com.etc.video.domain.Video;
import com.etc.video.service.impl.VideoFavServiceImpl;
import com.etc.video.service.VideoFavService;
import com.google.gson.Gson;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@WebServlet({"/video_collect.do", "/video_cancel_collect.do", "/show_video_collect.do", "/validate_collect.do","/count_collect_num.do"})
public class VideoFavServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String uri = request.getRequestURI();
        String action = uri.substring(uri.lastIndexOf("/") + 1);
        PrintWriter out = response.getWriter();
        VideoFavService service = new VideoFavServiceImpl();
        Map<String, Object> resultMap = new HashMap<>();
        if ("video_collect.do".equals(action)) {
            int uid = Integer.parseInt(request.getParameter("uid"));
            int vid = Integer.parseInt(request.getParameter("vdno"));
            int result = service.insert(uid, vid);
            if (result != -1) {
                resultMap.put("code", 200);
                resultMap.put("msg", "收藏成功!");
            } else {
                resultMap.put("code", 201);
                resultMap.put("msg", "收藏失败!");
            }
        } else if ("show_video_collect.do".equals(action)) {
            int uid = Integer.parseInt(request.getParameter("uid"));
            List<Video> vs = service.showCollectVideo(uid);
            if (vs.size() > 0) {
                resultMap.put("code", 200);
                resultMap.put("msg", "我的收藏列表展示");
                resultMap.put("result", vs);
            } else {
                resultMap.put("code", 201);
                resultMap.put("msg", "暂未收藏视频");
            }
        } else if ("video_cancel_collect.do".equals(action)) {
            int uid = Integer.parseInt(request.getParameter("uid"));
            int vid = Integer.parseInt(request.getParameter("vid"));
            int result = service.delete(uid, vid);
            if (result != -1) {
                resultMap.put("code", 200);
                resultMap.put("msg", "取消收藏!");
            } else {
                resultMap.put("code", 201);
                resultMap.put("msg", "取消收藏失败!");
            }

        } else if ("validate_collect.do".equals(action)) {
            int uid = Integer.parseInt(request.getParameter("uid"));
            int vid = Integer.parseInt(request.getParameter("vdno"));
            int result = service.validateCollect(uid, vid);
            System.out.println(uid+":"+vid+":"+result);
            if(result!=-1){
                resultMap.put("code",200);
            }else{
                resultMap.put("code",201);
            }
        }else if("count_collect_num.do".equals(action)){
            int vid = Integer.parseInt(request.getParameter("vid"));
            int count = service.countCollectNum(vid);
            System.out.println(count);
            resultMap.put("count",count);
        }
        Gson gson = new Gson();
        String mstr = gson.toJson(resultMap);
        out.print(mstr);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}
