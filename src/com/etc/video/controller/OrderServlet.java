package com.etc.video.controller;

import com.etc.video.domain.Order;
import com.etc.video.domain.User;
import com.etc.video.service.OrderService;
import com.etc.video.service.UserService;
import com.etc.video.service.impl.OrderServiceImpl;
import com.etc.video.service.impl.UserServiceImpl;
import com.google.gson.Gson;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.*;

@WebServlet({"/OrderServlet.do", "/getOrderByAccount.do", "/getAllOrder.do"})
public class OrderServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //订单处理
        HttpSession session = request.getSession();
        OrderService orderService = new OrderServiceImpl();
        UserService userService = new UserServiceImpl();
        String uri = request.getRequestURI();
        System.out.println(uri);
        String action = uri.substring(uri.lastIndexOf("/") + 1);
        Gson gson = new Gson();
        PrintWriter out = response.getWriter();
        Map resultMap = new HashMap<>();
        if ("OrderServlet.do".equals(action)) {
            String account = ((User) session.getAttribute("user")).getAccount();
            int ordertime = Integer.parseInt(request.getParameter("ordertime"));
            String ordername = null;
            Double orderprice = 0.0;
            if (ordertime == 1) {
                ordername = "一个月会员支付";
                orderprice = 24.0;
            } else if (ordertime == 3) {
                ordername = "一季度会员支付";
                orderprice = 70.0;
            } else if (ordertime == 6) {
                ordername = "6个月会员支付";
                orderprice = 120.0;
            } else if (ordertime == 12) {
                ordername = "一年会员支付";
                orderprice = 200.0;
            }

            System.out.println(ordername);

            //Double.parseDouble(request.getParameter("orderprice"));

            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd  HH:mm:ss ");
            //时间的操作
            //获取当前时间
            Date date = new Date(System.currentTimeMillis());
            String orderstart = simpleDateFormat.format(date);
            //获取vip的到期的时间
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            calendar.add(Calendar.MONDAY, ordertime);
            String orderend = simpleDateFormat.format(calendar.getTime());

            //随机生成10位的订单号
            Random random = new Random();
            String orderno = new String();
            for (int i = 0; i < 10; i++) {
                orderno += random.nextInt(10);
            }
            System.out.println(orderno);
            Order order = new Order();
            order.setOrderno(orderno);
            order.setAccount(account);
            order.setOrdername(ordername);
            order.setOrderprice(orderprice);
            order.setOrdertime(ordertime);
            order.setOrderstart(orderstart);
            order.setOrderend(orderend);
            int result1 = orderService.addOrder(order);
            Map<String, Object> map = new HashMap<>();
            if (result1 != -1) {
                System.out.println("插入成功");
                resultMap.put("code", 200);
                userService.updateVip(account);

            } else {
                resultMap.put("code", 201);
                System.out.println("失败");
            }
            //根据account查询order
        } else if ("getOrderByAccount".equals(action)) {
            String account = request.getParameter("account");
            Order order = orderService.selectOrder(account);
            if (order != null) {
                resultMap.put("msg", "查询成功");
                resultMap.put("code", "200");
                resultMap.put("order", order);
            } else {
                resultMap.put("msg", "查不到此订单");
                resultMap.put("code", "201");
            }
            //查询所有的订单
        } else if ("getAllOrder.do".equals(action)) {
            List<Order> orderList = orderService.getAllOrder();
            if (orderList != null) {
                resultMap.put("msg", "查询成功");
                resultMap.put("code", "200");
                resultMap.put("orderList", orderList);
            } else {
                resultMap.put("msg", "查不到此订单");
                resultMap.put("code", "201");
            }
        }

        response.getWriter().print(new Gson().toJson(resultMap));


    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}
