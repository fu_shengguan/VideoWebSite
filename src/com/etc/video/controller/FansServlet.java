package com.etc.video.controller;

import com.etc.video.domain.User;
import com.etc.video.domain.Video;
import com.etc.video.service.FansService;
import com.etc.video.service.VideoService;
import com.etc.video.service.impl.FansServiceImpl;
import com.etc.video.service.impl.VideoServiceImpl;
import com.google.gson.Gson;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@WebServlet({"/validate_follow.do","/follow.do","/cancel_follow.do","/query_follows.do"})
public class FansServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String uri = request.getRequestURI();
        String action = uri.substring(uri.lastIndexOf("/")+1);
        PrintWriter out = response.getWriter();
        FansService service = new FansServiceImpl();
        VideoService vservice = new VideoServiceImpl();
        Map<String,Object> resultMap = new HashMap<>();
        if("validate_follow.do".equals(action)){
            int cuid = Integer.parseInt(request.getParameter("cuid"));
            int vdno = Integer.parseInt(request.getParameter("vdno"));
            Video v = vservice.selectByVdno(vdno);
            int fuid = v.getUid();
            int result = service.validateFollow(cuid,fuid);
            if(result!=-1){
                resultMap.put("code",200);
                resultMap.put("msg","已关注");
            }else{
                resultMap.put("code",201);
                resultMap.put("msg","未关注");
            }
        }else if("follow.do".equals(action)){
            int fansid = Integer.parseInt(request.getParameter("cuid"));
            int vid = Integer.parseInt(request.getParameter("vdno"));
            Video v = vservice.selectByVdno(vid);
            int fuid = v.getUid();
            Boolean b = service.addFans(fansid,fuid);
            if(b){
                resultMap.put("code",200);
                resultMap.put("msg","关注成功!");
            }else{
                resultMap.put("code",201);
                resultMap.put("msg","关注失败!");
            }

        }else if("cancel_follow.do".equals(action)){
            int fansid = Integer.parseInt(request.getParameter("cuid"));
            int vid = Integer.parseInt(request.getParameter("vdno"));
            Video v = vservice.selectByVdno(vid);
            int fuid = v.getUid();
            Boolean b = service.deleteFans(fansid,fuid);
            if(b){
                resultMap.put("code",200);
                resultMap.put("msg","取消关注!");
            }else{
                resultMap.put("code",201);
                resultMap.put("msg","取消关注失败!");
            }
        }else if("query_follows.do".equals(action)){
            int uid = Integer.parseInt(request.getParameter("uid"));
            List<User> list = service.queryAuthorByUid(uid);
            if(list.size()>0){
                resultMap.put("code",200);
                resultMap.put("msg","查询成功");
                resultMap.put("result",list);
            }else{
                resultMap.put("code",201);
                resultMap.put("msg","还未关注视频作者哦!");

            }
        }
        Gson gson = new Gson();
        String str = gson.toJson(resultMap);
        out.print(str);

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request,response);
    }
}
