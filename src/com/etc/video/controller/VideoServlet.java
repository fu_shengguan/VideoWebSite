package com.etc.video.controller;

import com.etc.video.domain.Pager;
import com.etc.video.domain.User;
import com.etc.video.domain.Video;
import com.etc.video.domain.VideoLocal;
import com.etc.video.service.*;
import com.etc.video.service.impl.*;
import com.etc.video.tools.PageData;
import com.etc.video.util.MyRandom;
import com.google.gson.Gson;
import it.sauronsoftware.jave.Encoder;
import it.sauronsoftware.jave.MultimediaInfo;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import javax.lang.model.type.ArrayType;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.*;

@WebServlet({"/VideoServlet", "/select_video", "/query_type_page_video",
        "/query_video_rotation", "/query_all_num_video",
        "/add_video.do", "/query_all_video", "/query_video",
        "/update_video", "/delete_video", "/admin_update_video",
        "/query_all_page_video", "/update_state_video", "/author_video.do","/query_admin_all_page_video"})
public class VideoServlet extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        PrintWriter out = response.getWriter();
        Gson gson = new Gson();
        String uri = request.getRequestURI();
        String action = uri.substring(uri.lastIndexOf("/") + 1);
        VideoService videoService = new VideoServiceImpl();
        UserService userService = new UserServiceImpl();
        VideoFavService videoFavService = new VideoFavServiceImpl();
        OrderService orderService = new OrderServiceImpl();
        FansService fansService = new FansServiceImpl();
        VideoTypeService videoTypeService = new VideoTypeServiceImpl();
        DiskFileItemFactory factory = new DiskFileItemFactory();
        Map<String, Object> resultMap = new HashMap<>();
        ServletFileUpload upload = new ServletFileUpload(factory);
        upload.setHeaderEncoding("UTF-8");
        Video video = new Video();
        if ("add_video.do".equals(action)) {
            try {
                List<FileItem> items = upload.parseRequest(request);
                int uid = ((User) session.getAttribute("user")).getUid();
                video.setUid(uid);//上传者sessio 模拟一下
                Date date = new Date();
                SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
                System.out.println(formatter.format(date));
                video.setUpdatetime(formatter.format(date));


                String pathVideo = this.getServletContext().getRealPath("upload/video/");
                String pathVideoCover = this.getServletContext().getRealPath("upload/videopage/");
                File uploadFile1 = new File(pathVideo);
                File uploadFile2 = new File(pathVideoCover);
                //判断目录是否存在，不存在创建
                if (!uploadFile1.exists()) {
                    uploadFile1.mkdirs();
                    //mkdir(), mkdirs()
                }
                //判断目录是否存在，不存在创建
                if (!uploadFile2.exists()) {
                    uploadFile2.mkdirs();
                    //mkdir(), mkdirs()创建父目录
                }


                for (FileItem item : items) {

                    if (item.isFormField()) {
                        String val = new String(item.getString().getBytes("iso-8859-1"), "UTF-8");

                        if ("vdname".equals(item.getFieldName())) {
                            video.setVdname(val);//视频名称
                        } else if ("vdtypeid".equals(item.getFieldName())) {
                            video.setVdtypeid(Integer.parseInt(val));//视频类型
                        } else if ("summary".equals(item.getFieldName())) {
                            video.setSummary(val);//视频简介
                        } else if ("vip".equals(item.getFieldName())) {
                            video.setVip(Integer.parseInt(val));//视频会员限制
                        } else if ("state".equals(item.getFieldName())) {
                            video.setState(Integer.parseInt(val));//视频审核状态
                        } else if ("file_name".equals(item.getFieldName())) {
                            System.out.println(val);
                        }
                    } else {
                        DiskFileItem fileItem = (DiskFileItem) item;

                        if ("cover".equals(item.getFieldName())) {
                            File coverFile = new File(uploadFile2, fileItem.getName());
                            fileItem.write(coverFile);
                            video.setCover("/upload/videopage/" + coverFile.getName());//封面地址

                        } else {
                            if ("file".equals(item.getFieldName())) {
                                Encoder encoder = new Encoder();
                                System.out.println(fileItem.getStoreLocation());
                                MultimediaInfo m = encoder.getInfo(fileItem.getStoreLocation());
                                long ls = m.getDuration() / 1000;
                                int hour = (int) (ls / 3600);
                                int minute = (int) (ls % 3600) / 60;
                                int second = (int) (ls - hour * 3600 - minute * 60);
                                if (hour == 0) {
                                    video.setVideotime(minute + ":" + second);//视频时长

                                } else {
                                    video.setVideotime(hour + ":" + minute + ":" + second);//视频时长
                                }


                                File coverFile = new File(uploadFile1, fileItem.getName());

                                System.out.println("/upload/video/" + coverFile.getName());
                                video.setAddress("/upload/video/" + coverFile.getName());//视频地址
                                fileItem.write(coverFile);
                                Boolean bl = videoService.addVideo(video);
                                if (bl) {
                                    //resultMap.put("vdno", 200);
                                    resultMap.put("msg", "上传视频成功");
                                    System.out.println("上传视频成功");
                                } else {
                                    resultMap.put("code", 201);
                                    resultMap.put("msg", "上传视频失败");
                                    System.out.println("上传视频失败");
                                    coverFile.delete();

                                }


                            }

                        }
                    }


                }

            } catch (Exception e) {
                e.printStackTrace();
            }


        } else if ("admin_update_video".equals(action)) {

            int vdno = Integer.parseInt(request.getParameter("vdno"));
            String vdname = request.getParameter("vdname");
            int vdtypeid = Integer.parseInt(request.getParameter("vdtypeid"));
            String summary = request.getParameter("summary");
            String cover = request.getParameter("cover");
            int uid = Integer.parseInt(request.getParameter("uid"));
            String updatetime = request.getParameter("updatetime");
            int vip = Integer.parseInt(request.getParameter("vip"));
            int state = Integer.parseInt(request.getParameter("state"));
            String address = request.getParameter("address");
            String videotime = request.getParameter("videotime");

            Video video1 = new Video(vdno, vdname, vdtypeid, summary, cover, uid, updatetime, vip, state, address, videotime);
            Boolean b1 = videoService.updateVideo(vdno, video1);
            if (b1) {
                resultMap.put("code", 200);
                resultMap.put("msg", "管理员更新视频成功");
                System.out.println("管理员更新视频成功");
            } else {
                resultMap.put("code", 201);
                resultMap.put("msg", "管理员更新失败");
                System.out.println("管理员更新失败");
            }


        } else if ("query_all_video".equals(action)) {
            List<Video> list = videoService.queryAllVideo();
            List<Video> list2 = new ArrayList<>();
            List<VideoLocal> listVideoLocal = new ArrayList<>();
            for (Video video2 : list) {
                if(video2.getState()==1){
                        list2.add(video2);
                }
            }
            for (Video video1 : list2) {
                VideoLocal videoLocal = new VideoLocal(video1.getVdno(), video1.getVdname(), videoTypeService.
                        selectByVdTypeId(video1.getVdtypeid()),
                        video1.getSummary(), video1.getCover(), userService.selectByUid(video1.getUid()),
                        video1.getUpdatetime(), video1.getVip() == 0 ? "普通用户可看" : "会员可看"
                        , video1.getState() == 0 ? "待审核" : video1.getState() == 1 ? "审核通过" : "审核未通过",
                        video1.getAddress(), video1.getVideotime(), fansService.quaryFansCountBuUid(video1.getUid()));
                listVideoLocal.add(videoLocal);
            }
            if (!list.isEmpty()) {
                resultMap.put("code", 200);
                resultMap.put("msg", "查询视频成功");
                resultMap.put("result", list);
                resultMap.put("resultLocal", listVideoLocal);
                System.out.println("查询视频成功");
            } else {
                resultMap.put("code", 201);
                resultMap.put("msg", "查询失败");
                System.out.println("查询失败");
            }

        } else if ("query_admin_all_video".equals(action)) {
            List<Video> list = videoService.queryAllVideo();
            List<Video> list2 = new ArrayList<>();
            List<VideoLocal> listVideoLocal = new ArrayList<>();
            for (Video video1 : list) {
                VideoLocal videoLocal = new VideoLocal(video1.getVdno(), video1.getVdname(), videoTypeService.
                        selectByVdTypeId(video1.getVdtypeid()),
                        video1.getSummary(), video1.getCover(), userService.selectByUid(video1.getUid()),
                        video1.getUpdatetime(), video1.getVip() == 0 ? "普通用户可看" : "会员可看"
                        , video1.getState() == 0 ? "待审核" : video1.getState() == 1 ? "审核通过" : "审核未通过",
                        video1.getAddress(), video1.getVideotime(), fansService.quaryFansCountBuUid(video1.getUid()));
                listVideoLocal.add(videoLocal);
            }
            if (!list.isEmpty()) {
                resultMap.put("code", 200);
                resultMap.put("msg", "查询视频成功");
                resultMap.put("result", list);
                resultMap.put("resultLocal", listVideoLocal);
                System.out.println("查询视频成功");
            } else {
                resultMap.put("code", 201);
                resultMap.put("msg", "查询失败");
                System.out.println("查询失败");
            }

        } else if ("query_all_page_video".equals(action)) {
            //分页查询
            String name = request.getParameter("name");
            System.out.println("关键字是：" + name);
            int currPage = 1;
            int state = 1;
            int pageNum = 8;
            try {
                currPage = Integer.parseInt(request.getParameter("currPage"));
            } catch (Exception e) {
            }
            PageData<Video> pageData = videoService.selectByVdname(name,state, currPage, pageNum);
            List<Video> list = pageData.getData();
            List<Video> list2 = new ArrayList<>();
            for (Video video2 : list) {
                if(video2.getState()==1){
                    list2.add(video2);
                }
            }
            List<VideoLocal> listVideoLocal = new ArrayList<>();
            for (Video video1 : list2) {
                VideoLocal videoLocal = new VideoLocal(video1.getVdno(), video1.getVdname(), videoTypeService.
                        selectByVdTypeId(video1.getVdtypeid()),
                        video1.getSummary(), video1.getCover(), userService.selectByUid(video1.getUid()),
                        video1.getUpdatetime(), video1.getVip() == 0 ? "普通用户可看" : "会员可看"
                        , video1.getState() == 0 ? "待审核" : video1.getState() == 1 ? "审核通过" : "审核未通过",
                        video1.getAddress(), video1.getVideotime(), fansService.quaryFansCountBuUid(video1.getUid()));
                listVideoLocal.add(videoLocal);
            }
            if (!list.isEmpty()) {
                resultMap.put("code", 200);
                resultMap.put("msg", "查询视频成功");
                resultMap.put("result", list);
                resultMap.put("page", pageData);
                System.out.println(pageData.toString());
                resultMap.put("resultLocal", listVideoLocal);
                System.out.println("查询视频成功");
            } else {
                resultMap.put("code", 201);
                resultMap.put("msg", "查询失败");
                System.out.println("查询失败");
            }
        } else if ("query_admin_all_page_video".equals(action)) {
            //分页查询
            String name = request.getParameter("name");
            System.out.println("关键字是：" + name);
            int currPage = 1;
            int state = 1;
            int pageNum = 8;
            try {
                currPage = Integer.parseInt(request.getParameter("currPage"));
            } catch (Exception e) {
            }
            PageData<Video> pageData = videoService.adminSelectByVdname(name, currPage, pageNum);
            List<Video> list = pageData.getData();


            List<VideoLocal> listVideoLocal = new ArrayList<>();
            for (Video video1 : list) {
                VideoLocal videoLocal = new VideoLocal(video1.getVdno(), video1.getVdname(), videoTypeService.
                        selectByVdTypeId(video1.getVdtypeid()),
                        video1.getSummary(), video1.getCover(), userService.selectByUid(video1.getUid()),
                        video1.getUpdatetime(), video1.getVip() == 0 ? "普通用户可看" : "会员可看"
                        , video1.getState() == 0 ? "待审核" : video1.getState() == 1 ? "审核通过" : "审核未通过",
                        video1.getAddress(), video1.getVideotime(), fansService.quaryFansCountBuUid(video1.getUid()));
                listVideoLocal.add(videoLocal);
            }
            if (!list.isEmpty()) {
                resultMap.put("code", 200);
                resultMap.put("msg", "查询视频成功");
                resultMap.put("result", list);
                resultMap.put("page", pageData);
                System.out.println(pageData.toString());
                resultMap.put("resultLocal", listVideoLocal);
                System.out.println("查询视频成功");
            } else {
                resultMap.put("code", 201);
                resultMap.put("msg", "查询失败");
                System.out.println("查询失败");
            }
        } else   if ("query_all_num_video".equals(action)) {
            List<Video> list = videoService.queryAllVideo();
            List<Video> list2 = new ArrayList<>();
            for (Video video2 : list) {
                if(video2.getState()==1){
                    list2.add(video2);
                }
            }
            resultMap.put("num", list2.size());
        } else if ("query_video_rotation".equals(action)) {
            List<Video> list = videoService.queryAllVideo();
            List<VideoLocal> listVideoLocal = new ArrayList<>();
            List<Video> list2 = new ArrayList<>();
            for (Video video2 : list) {
                if(video2.getState()==1){
                    list2.add(video2);
                }
            }
            for (Video video1 : list2) {
                VideoLocal videoLocal = new VideoLocal(video1.getVdno(), video1.getVdname(), videoTypeService.
                        selectByVdTypeId(video1.getVdtypeid()),
                        video1.getSummary(), video1.getCover(), userService.selectByUid(video1.getUid()),
                        video1.getUpdatetime(), video1.getVip() == 0 ? "普通用户可看" : "会员可看"
                        , video1.getState() == 0 ? "待审核" : video1.getState() == 1 ? "审核通过" : "审核未通过",
                        video1.getAddress(), video1.getVideotime(), fansService.quaryFansCountBuUid(video1.getUid()));
                listVideoLocal.add(videoLocal);
            }
            List<VideoLocal> list1 = new ArrayList<>();
            int a[] = new int[4];
            a =MyRandom.getRandoms(0,list2.size(),4);
            System.out.println(Arrays.toString(a));
       /*     a[0] = (int) (Math.random() * list2.size());
            boolean isFind = false;*/
/*            for (int i = 1; i < 4; i++) {
                int tempnum = (int) (Math.random() * list2.size());

                for (int j = 0; j < i; j++) {
                    if (a[j] == tempnum) {
                        isFind = true;
                    }
                }
                if (!isFind) {
                    System.out.println(tempnum);
                    a[i] = tempnum;
                } else {
                    i--;
                }
            }*/

            for (int i = 0; i < a.length; i++) {
                list1.add(listVideoLocal.get(a[i % 4]));
                i++;
                list1.add(listVideoLocal.get(a[i % 4]));
                i++;
                list1.add(listVideoLocal.get(a[i % 4]));
                i++;
                list1.add(listVideoLocal.get(a[i % 4]));
                i = i - 3;
            }

            resultMap.put("rotation", list1);
        } else if ("select_video".equals(action)) {
            int vdno = Integer.parseInt(request.getParameter("vdno"));

            Video video2 = videoService.selectByVdno(vdno);
            User user = (User) session.getAttribute("user");
            System.out.println(video2.toString());
            VideoLocal videoLocal = new VideoLocal(video2.getVdno(), video2.getVdname(), videoTypeService.selectByVdTypeId(video2.getVdtypeid()),
                    video2.getSummary(), video2.getCover(), userService.selectByUid(video2.getUid()),
                    video2.getUpdatetime(), video2.getVip() == 0 ? "" : "会员可看"
                    , video2.getState() == 0 ? "待审核" : video2.getState() == 1 ? "审核通过" : "审核未通过",
                    video2.getAddress(), video2.getVideotime(), fansService.quaryFansCountBuUid(video2.getUid()));
            System.out.println(videoLocal.toString());
            if (video2.toString() != null) {
                System.out.println(videoLocal.toString());
                resultMap.put("result", video2);
                resultMap.put("videoLocal", videoLocal);
                resultMap.put("code", 200);
                resultMap.put("msg", "查询视频成功");
            } else {
                resultMap.put("code", 201);
                resultMap.put("msg", "查询视频失败");
            }

        } else if ("update_state_video".equals(action)) {
            int vdno = Integer.parseInt(request.getParameter("vdno"));
            int state = Integer.parseInt(request.getParameter("state"));
            //Video video2=new Video(vdno,state);
            Boolean b1 = videoService.updateState(vdno, state);
            if (b1) {
                resultMap.put("code", 200);
                resultMap.put("msg", "成功");
            } else {
                resultMap.put("code", 201);
                resultMap.put("msg", "失败");
            }


        } else if ("query_type_page_video".equals(action)) {
            //分页查询
            int vdtypeid = Integer.parseInt(request.getParameter("vdtypeid"));
            System.out.println("查询类型：" + vdtypeid);
            int currPage = 1;
            int pageNum = 8;
            try {
                currPage = Integer.parseInt(request.getParameter("currPage"));
            } catch (Exception e) {
            }
            PageData<Video> pageData = videoService.selectByVdtypeid(vdtypeid, currPage, pageNum);
            List<Video> list = pageData.getData();
            List<VideoLocal> listVideoLocal = new ArrayList<>();
            List<Video> list2 = new ArrayList<>();
            for (Video video2 : list) {
                if(video2.getState()==1){
                    list2.add(video2);
                }
            }
            for (Video video1 : list2) {
                VideoLocal videoLocal = new VideoLocal(video1.getVdno(), video1.getVdname(), videoTypeService.
                        selectByVdTypeId(video1.getVdtypeid()),
                        video1.getSummary(), video1.getCover(), userService.selectByUid(video1.getUid()),
                        video1.getUpdatetime(), video1.getVip() == 0 ? "普通用户可看" : "会员可看"
                        , video1.getState() == 0 ? "待审核" : video1.getState() == 1 ? "审核通过" : "审核未通过",
                        video1.getAddress(), video1.getVideotime(), fansService.quaryFansCountBuUid(video1.getUid()));
                listVideoLocal.add(videoLocal);
            }
            if (!list.isEmpty()) {
                resultMap.put("code", 200);
                resultMap.put("msg", "查询视频成功");
                resultMap.put("result", list);
                resultMap.put("page", pageData);
                System.out.println(pageData.toString());
                resultMap.put("resultLocal", listVideoLocal);
                System.out.println("查询视频成功");
            } else {
                resultMap.put("code", 201);
                resultMap.put("msg", "查询失败");
                System.out.println("查询失败");
            }

        } else if ("author_video.do".equals(action)) {
            System.out.println(request.getParameter("vdno"));
            int vdno = Integer.parseInt(request.getParameter("vdno"));
            Video video3 = videoService.selectByVdno(vdno);
            UserService userService2 = new UserServiceImpl();
            List<Video> listvideo1 = videoService.selectByUidNoPage(video3.getUid());
            List<VideoLocal> listVideoLocal1 = new ArrayList<>();
            List<Video> list2 = new ArrayList<>();
            for (Video video2 : listvideo1) {
                if(video2.getState()==1){
                    list2.add(video2);
                }
            }
            for (Video video1 : list2) {
                VideoLocal videoLocal = new VideoLocal(video1.getVdno(), video1.getVdname(), videoTypeService.
                        selectByVdTypeId(video1.getVdtypeid()),
                        video1.getSummary(), video1.getCover(), userService.selectByUid(video1.getUid()),
                        video1.getUpdatetime(), video1.getVip() == 0 ? "普通用户可看" : "会员可看"
                        , video1.getState() == 0 ? "待审核" : video1.getState() == 1 ? "审核通过" : "审核未通过",
                        video1.getAddress(), video1.getVideotime(), fansService.quaryFansCountBuUid(video1.getUid()));
                listVideoLocal1.add(videoLocal);
            }
            User user = userService.queryUserByUid(video3.getUid());
            List<Video> listvideo2 = videoFavService.showCollectVideo(user.getUid());
            List<VideoLocal> listVideoLocal2 = new ArrayList<>();
            List<Video> list3 = new ArrayList<>();
            for (Video video2 : listvideo2) {
                if(video2.getState()==1){
                    list3.add(video2);
                }
            }
            for (Video video1 : list3) {
                VideoLocal videoLocal = new VideoLocal(video1.getVdno(), video1.getVdname(), videoTypeService.
                        selectByVdTypeId(video1.getVdtypeid()),
                        video1.getSummary(), video1.getCover(), userService.selectByUid(video1.getUid()),
                        video1.getUpdatetime(), video1.getVip() == 0 ? "普通用户可看" : "会员可看"
                        , video1.getState() == 0 ? "待审核" : video1.getState() == 1 ? "审核通过" : "审核未通过",
                        video1.getAddress(), video1.getVideotime(), fansService.quaryFansCountBuUid(video1.getUid()));
                listVideoLocal2.add(videoLocal);
            }
            List<User> author = fansService.queryAuthorByUid(video3.getUid());
            List<User> fans = fansService.queryFansByUid(video3.getUid());
            HttpSession httpSession = request.getSession();
            User user1 = (User) httpSession.getAttribute("user");
            System.out.println(user1);
            if (user1.getUid() == video3.getUid()) {
                //个人信息
                resultMap.put("user", user);
                //收藏作品
                resultMap.put("collect", listVideoLocal2);
                //粉丝
                resultMap.put("fans", fans);
                //关注作者
                resultMap.put("author", author);
            }

            //个人作品
            resultMap.put("video", listVideoLocal1);


        }


        response.getWriter().print(gson.toJson(resultMap));
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}
