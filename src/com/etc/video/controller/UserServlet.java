package com.etc.video.controller;

import com.etc.video.domain.User;
import com.etc.video.service.UserService;
import com.etc.video.service.impl.UserServiceImpl;
import com.google.gson.Gson;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@WebServlet({"/userregister.do","/zhuxiao", "/userlogin.do", "/checkAccount.do", "/regexpPassword.do", "/showInfo.do", "/updateInfo.do", "/queryAllUserByuname.do", "/queryAllUser.do","/dependVip.do"})
public class UserServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        UserService userService = new UserServiceImpl();
        //得到当前的地址
        String uri = request.getRequestURI();
        //System.out.println(uri);
        String action = uri.substring(uri.lastIndexOf("/") + 1);
        //System.out.println(action);
        //向前端返回数据
        PrintWriter out = response.getWriter();
        Gson gson = new Gson();
        Map<String, Object> resultMap = new HashMap<>();
        if ("userregister.do".equals(action)) {
            String account = request.getParameter("account");
            String password = request.getParameter("password");
            User user = new User(account, password);
            int result = userService.addUser(user);
            if (result != -1) {
                resultMap.put("code", 200);
                resultMap.put("msg", "注册成功");

                //out.print("success");
            } else {
                resultMap.put("code", 201);
                resultMap.put("msg", "注册失败");
                out.print(result);
            }

        } else if ("userlogin.do".equals(action)) {
            String account = request.getParameter("account");
            String password = request.getParameter("password");
            User user = userService.login(account);
            session.setAttribute("user", user);

            if (user == null) {
                resultMap.put("code", "202");
                resultMap.put("msg", "账号不存在");
            } else if (!user.getUpwd().equals(password)) {
                resultMap.put("code", "201");
                resultMap.put("msg", "密码输入有误");

            } else {
                resultMap.put("code", "200");
                resultMap.put("msg", "登录成功");
                resultMap.put("result", user);
                session.setAttribute("user", user);
            }
        } else if ("checkAccount.do".equals(action)) {
            String account = request.getParameter("account");

            User user = userService.login(account);
            if (user != null) {
                resultMap.put("code", 201);
                resultMap.put("msg", "该账号已被占用");
                resultMap.put("user",user);
            } else {
                resultMap.put("code", 200);
                resultMap.put("msg", "该账号可用");
            }

        } else if ("showInfo.do".equals(action)) {
            String account = ((User)session.getAttribute("user")).getAccount();
            User user = userService.login(account);
            //System.out.println(user);
            resultMap.put("user", user);


        } else if ("updateInfo.do".equals(action)) {

            String account = ((User)session.getAttribute("user")).getAccount();
            String uname = request.getParameter("uname");
            String phone = request.getParameter("phone");
            String email = request.getParameter("email");
            //cString uimg = request.getParameter("uimg");
            String birth = request.getParameter("birth");
            User user = (User)session.getAttribute("user");

            user.setUname(uname);
            user.setPhone(phone);
            user.setEmail(email);
            //user.setUimg(uimg);
            user.setBirth(birth);

            System.out.println(user.getEmail());


            int c = 0;
            c = userService.updateInfo(user);
            if (c != -1) {
                resultMap.put("msg", "修改成功");
                resultMap.put("code", "200");
                User user1 = userService.login(account);
                resultMap.put("user", user1);
            } else {
                resultMap.put("msg", "修改失败");
                resultMap.put("code", "201");
            }


        } else if ("queryAllUserByuname.do".equals(action)) {
            String uname = request.getParameter("uname");
            List<User> list = userService.selectUserByLike("uname");
            if (list.isEmpty() != true) {
                resultMap.put("msg", "查询成功");
                resultMap.put("code", "200");

                resultMap.put("user", list);
            } else {
                resultMap.put("msg", "查询失败,没有这个用户");
                resultMap.put("code", "201");
            }

        } else if ("queryAllUser.do".equals(action)) {

            List<User> list = userService.getAllUser();
            if (list != null) {
                resultMap.put("msg", "查询成功");
                resultMap.put("code", "200");

                resultMap.put("user", list);
            } else {
                resultMap.put("msg", "查询失败,没有这个用户");
                resultMap.put("code", "201");
            }
        }else if ("dependVip.do".equals(action)){
            String account = request.getParameter("account");
            System.out.println(account);
            User user = userService.selectVip(account.trim());
            System.out.println(user);

            if (user.getState()==1){
                resultMap.put("msg","100");
                resultMap.put("name","普通用户");
            }else if (user.getState()==2){
                resultMap.put("msg","150");
                resultMap.put("name","会员用户");
            }
        }else if("zhuxiao".equals(action)){
            session.removeAttribute("user");
            session.invalidate();
            System.out.println("注销成功");
            resultMap.put("msg","注销成功");
        }
        String str = gson.toJson(resultMap);
        //System.out.println(str);
        out.print(str);

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}
