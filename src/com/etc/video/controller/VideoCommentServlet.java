package com.etc.video.controller;

import com.etc.video.domain.Video;
import com.etc.video.domain.VideoComment;
import com.etc.video.domain.VideoCommentLocal;
import com.etc.video.service.UserService;
import com.etc.video.service.VideoCommentService;
import com.etc.video.service.impl.UserServiceImpl;
import com.etc.video.service.impl.VideoCommentServiceImpl;
import com.google.gson.Gson;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.*;

@WebServlet({"/content.action","/queryAllComments.action"})
//控制层
public class VideoCommentServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        //调用 VideoCommentServiceImpl 中的方法
        VideoCommentService videoCommentService=new VideoCommentServiceImpl();
        //向前台输出内容
        PrintWriter out=response.getWriter();
        //从前端获取内容
        //判断请求地址
        String uri=request.getRequestURI();

        Gson gson=new Gson();

          //截取路径
          String action=uri.substring(uri.lastIndexOf("/")+1);
          Map<String,Object> resultMap=new HashMap<>();
          if("content.action".equals(action)){
              //发布评论
              String content=request.getParameter("content");
              //获取评论视频编号
              int vdno=Integer.parseInt(request.getParameter("vdno"));
              int cuid= Integer.parseInt(request.getParameter("cuid"));
              System.out.println("添加一条评论"+content+"+"+vdno);
              //int cstar = Integer.parseInt(request.getParameter("cstar"));
              //System.out.println(cstar);
             //获取当前系统时间
              Date date =new Date(System.currentTimeMillis());
              SimpleDateFormat simpleDateFormat=new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
              String ctime = simpleDateFormat.format(date);
              VideoComment videoComment=new VideoComment(content,0,ctime,cuid,vdno);//,cuid,vdno
              int result=videoCommentService.addVideoComment(videoComment);
              if(result !=-1){
                  resultMap.put("code",200);
                  resultMap.put("msg","发布评论成功");
              }else{
                  resultMap.put("code",201);
                  resultMap.put("msg","发布评论失败");
              }
              System.out.println("result ="+resultMap);

              String str = gson.toJson(resultMap);
              System.out.println("str="+str);
               out.print(str);
          }else   if("queryAllComments.action".equals(action)){
              int vdno= Integer.parseInt(request.getParameter("vdno"));
              UserService userService = new UserServiceImpl();
              VideoCommentService videoCommentService1 = new VideoCommentServiceImpl();
              List<VideoComment> list =  videoCommentService1.queryAllComments(vdno);
              List<VideoCommentLocal> listLocal =  new ArrayList<>();
              for (VideoComment vc : list) {
                        VideoCommentLocal vl = new VideoCommentLocal(vc.getCid(),vc.getContent(),vc.getCtime(),
                                userService.selectByUid(vc.getCuid()),vc.getCstar(),vc.getVdno());
                        listLocal.add(vl);
              }
              resultMap.put("listLocal",listLocal);
              resultMap.put("msg","查询评论成功");
              resultMap.put("code",200);
              String str = gson.toJson(resultMap);
              System.out.println("str="+str);
              out.print(str);
          }


        /*if("content.action".equals(action)){
            //删除评论
            int cid=Integer.parseInt(request.getParameter("cid"));

            System.out.println(cid);

            VideoComment videoComment=new VideoComment(cid);
            int result=videoCommentService.delVideoComment(videoComment);
            //检验
            if(result !=-1){
                //out.print("success");
                resultMap.put("code",200);
                resultMap.put("msg","发布成功");
                resultMap.put("cid",cid);
            }else{
                //out.print(result);
                resultMap.put("code",201);
                resultMap.put("msg","发布失败");
            }
            System.out.println("result ="+resultMap);

            String str = gson.toJson(resultMap);
            System.out.println("str="+str);
            out.print(str);
        }*/
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
         doPost(request,response);
    }
}
