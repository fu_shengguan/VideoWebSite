package com.etc.video.controller;

import com.etc.video.domain.Admin;
import com.etc.video.service.AdminSrevice;
import com.etc.video.service.impl.AdminServiceImpl;
import com.google.gson.Gson;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@WebServlet({"/registera.do", "/login.do", "/checkAccounts.do"})
public class AdminServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        AdminSrevice adminSrevice=new AdminServiceImpl();

        PrintWriter out = response.getWriter();

        // /MaoKe/register.do
        String uri = request.getRequestURI();

        Gson gosn = new Gson();

        String action = uri.substring(uri.lastIndexOf("/") + 1);
        Map<String, Object> resultMap = new HashMap<>();
       if ("register.do".equals(action)) {
            //执行注册操作
            String admname = request.getParameter("admname");
            String admpwd = request.getParameter("admpwd");

            Admin admin = new Admin();
            int result = adminSrevice.addadmin(admin);
            if (result != -1) {
                resultMap.put("code", 200);
                resultMap.put("msg", "注册成功");
            } else {
                resultMap.put("code", 201);
                resultMap.put("msg", "注册失败");
            }

        } else if ("login.do".equals(action)) {
            String admname = request.getParameter("admname");
            String admpwd = request.getParameter("admpwd");

            Admin admin=adminSrevice.login(admname);
            if (admin == null) {
                resultMap.put("code", 202);
                resultMap.put("msg", "账号不存在");
            } else if (!admin.getAdmpwd().equals(admpwd)) {
                resultMap.put("code", 201);
                resultMap.put("msg", "密码输入有误");
            } else {
                resultMap.put("code", 200);
                resultMap.put("msg", "登录成功");
                session.setAttribute("admin", admin);
                resultMap.put("result", admin);
            }
        } else if ("checkAccount.do".equals(action)) {
            String admname = request.getParameter("admname");
            Admin admin=adminSrevice.login(admname);
            if (admin != null) {
                resultMap.put("code", 201);
                resultMap.put("msg", "账号已经存在，请更换");
            } else {
                resultMap.put("code", 200);
                resultMap.put("msg", "账号可以使用");
            }
        }
        System.out.println("resultMap = " + resultMap);
        String str = gosn.toJson(resultMap);
        System.out.println("str = " + str);
        out.print(str);

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}