package com.etc.video.controller;

import com.etc.video.domain.Admin;
import com.etc.video.service.AdminSrevice;
import com.etc.video.service.impl.AdminServiceImpl;
import com.google.gson.Gson;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

@WebServlet("/AdminnServlet")
public class AdminnServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        AdminSrevice adminSrevice = new AdminServiceImpl();
        Gson gosn = new Gson();
        PrintWriter out = response.getWriter();
        List<Admin> list = adminSrevice.getadmins();

        String str = gosn.toJson(list);
        System.out.println(str);
        out.print(str);

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
            doPost(request,response);
    }
}
