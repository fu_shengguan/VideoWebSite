/*
SQLyog Ultimate v12.09 (64 bit)
MySQL - 5.6.24 : Database - db_vb
*********************************************************************
*/


/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`db_vb` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `db_vb`;

/*Table structure for table `tab_admin` */

DROP TABLE IF EXISTS `tab_admin`;

CREATE TABLE `tab_admin` (
                           `admid` int(10) NOT NULL AUTO_INCREMENT COMMENT '管理员id',
                           `admname` varchar(50) DEFAULT NULL COMMENT '管理员姓名',
                           `admpwd` varchar(50) DEFAULT NULL COMMENT '管理员密码',
                           PRIMARY KEY (`admid`),
                           UNIQUE KEY `admid` (`admid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `tab_admin` */

/*Table structure for table `tab_comment` */

DROP TABLE IF EXISTS `tab_comment`;

CREATE TABLE `tab_comment` (
                             `cid` int(10) NOT NULL COMMENT '评论编号',
                             `ccontent` varchar(200) DEFAULT NULL COMMENT '评论内容',
                             `ctime` date DEFAULT NULL COMMENT '发布该评论的时间',
                             `cuid` int(10) DEFAULT NULL COMMENT '评论改视频用户的id',
                             `cstar` int(5) DEFAULT NULL COMMENT '星级',
                             `vdno` int(11) DEFAULT NULL COMMENT '被评论视频的编号',
                             PRIMARY KEY (`cid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `tab_comment` */

/*Table structure for table `tab_fans` */

DROP TABLE IF EXISTS `tab_fans`;

CREATE TABLE `tab_fans` (
                          `fid` int(10) NOT NULL AUTO_INCREMENT COMMENT '关注编号',
                          `fuid` int(10) NOT NULL COMMENT '作者用户编号',
                          `fanid` int(10) NOT NULL COMMENT '粉丝用户编号',
                          PRIMARY KEY (`fid`),
                          UNIQUE KEY `fid` (`fid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `tab_fans` */

/*Table structure for table `tab_fav` */

DROP TABLE IF EXISTS `tab_fav`;

CREATE TABLE `tab_fav` (
                         `no` int(10) NOT NULL AUTO_INCREMENT COMMENT '记录编号',
                         `uid` int(10) NOT NULL COMMENT '用户编号',
                         `vdno` int(10) NOT NULL COMMENT '视频编号',
                         `collectime` date DEFAULT NULL COMMENT '收藏时间',
                         PRIMARY KEY (`no`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `tab_fav` */

/*Table structure for table `tab_order` */

DROP TABLE IF EXISTS `tab_order`;

CREATE TABLE `tab_order` (
                           `orderid` int(10) NOT NULL AUTO_INCREMENT COMMENT '订单表记录的号码',
                           `orderno` varchar(100) DEFAULT NULL COMMENT '订单编号(唯一)',
                           `ordername` varchar(50) DEFAULT NULL COMMENT '订单名称',
                           `orderprice` double(5,2) DEFAULT NULL COMMENT '订单金额',
                           `orderstart` datetime DEFAULT NULL COMMENT '订购日期',
                           `ordertime` int(5) DEFAULT NULL COMMENT '订购时长',
                           `orderend` datetime DEFAULT NULL COMMENT '订单结束日期',
                           `uid` int(10) DEFAULT NULL COMMENT '用户id',
                           PRIMARY KEY (`orderid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `tab_order` */

/*Table structure for table `tab_user` */

DROP TABLE IF EXISTS `tab_user`;

CREATE TABLE `tab_user` (
                          `uid` int(10) NOT NULL AUTO_INCREMENT COMMENT '用户编号',
                          `account` varchar(20) DEFAULT NULL COMMENT '用户账号',
                          `upwd` varchar(20) NOT NULL COMMENT '用户密码',
                          `uname` varchar(10) NOT NULL COMMENT '昵称',
                          `phone` varchar(11) DEFAULT NULL COMMENT '手机号',
                          `email` varchar(25) DEFAULT NULL COMMENT '电子邮件',
                          `state` int(3) DEFAULT '1' COMMENT '0管理员 1:用户 2: 会员',
                          `uimg` varchar(100) DEFAULT NULL COMMENT '用户头像',
                          `birth` date DEFAULT NULL COMMENT '生日',
                          `temp1` varchar(100) DEFAULT NULL COMMENT '附加字段',
                          `fancount` int(10) DEFAULT NULL COMMENT '粉丝数',
                          `available` int(10) DEFAULT '1' COMMENT '账号是否可用 0:禁用 1:可用',
                          PRIMARY KEY (`uid`),
                          UNIQUE KEY `UidP` (`uid`),
                          UNIQUE KEY `uni` (`account`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `tab_user` */

/*Table structure for table `tab_video` */

DROP TABLE IF EXISTS `tab_video`;

CREATE TABLE `tab_video` (
                           `vdno` int(10) NOT NULL AUTO_INCREMENT COMMENT '视频编号',
                           `vdname` varchar(100) NOT NULL COMMENT '视频名称',
                           `vdtypeid` int(10) NOT NULL COMMENT '视频类别编号',
                           `summary` text COMMENT '视频简介',
                           `cover` varchar(100) DEFAULT NULL COMMENT '视频封面',
                           `uid` varchar(100) DEFAULT NULL COMMENT '上传者id',
                           `updatetime` date DEFAULT NULL COMMENT '上传时间',
                           `limit` int(2) DEFAULT '0' COMMENT '0:普通用户可看 1:会员可看',
                           `status` int(2) DEFAULT '0' COMMENT '0:待审核,1:审核通过 2:审核未通过',
                           PRIMARY KEY (`vdno`),
                           UNIQUE KEY `uni` (`vdtypeid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `tab_video` */

/*Table structure for table `tab_video_type` */

DROP TABLE IF EXISTS `tab_video_type`;

CREATE TABLE `tab_video_type` (
                                `vdtypeid` int(10) NOT NULL AUTO_INCREMENT COMMENT '视频类别编号',
                                `vdtypename` varchar(200) NOT NULL,
                                PRIMARY KEY (`vdtypeid`),
                                UNIQUE KEY `vdtypeid` (`vdtypeid`),
                                UNIQUE KEY `vdtypename` (`vdtypename`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `tab_video_type` */

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
