package com.etc.test;

import com.etc.video.domain.Video;
import com.etc.video.service.VideoService;
import com.etc.video.service.impl.VideoServiceImpl;
import com.etc.video.tools.PageData;
import org.junit.Test;

import java.util.List;

public class VideoTest {
    @Test
    public void videoPage(){
        VideoService videoService = new VideoServiceImpl();
        PageData<Video> pageData   =videoService.selectByVdname("舞蹈",1,1,4);
        List<Video> list= pageData.getData();
        for(Video video:list){
            System.out.println( video.toString());
        };
    }
}
