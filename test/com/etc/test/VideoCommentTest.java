package com.etc.test;

import com.etc.video.dao.VideoCommentDao;
import com.etc.video.dao.impl.VideoCommentDaoImpl;
import com.etc.video.domain.VideoComment;
import com.etc.video.service.VideoCommentService;
import com.etc.video.service.impl.VideoCommentServiceImpl;
import org.junit.Test;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class VideoCommentTest {
    @Test
    public void testAddVideoComment() {
        VideoCommentDao videoCommentDao = new VideoCommentDaoImpl();
        Date date = new Date(System.currentTimeMillis());
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

        String ctime = simpleDateFormat.format(date);
        System.out.println(ctime);

        //VideoComment videoComment=new VideoComment("这片子的导演脑子被门挤了吧，什么烂片毫无看点",1,ctime);//,"2020-20-02"
        VideoComment videoComment = new VideoComment();
        videoComment.setContent("kk");
        videoComment.setCstar(1);
        videoComment.setCtime(ctime);
        int result = videoCommentDao.addVideoComment(videoComment);
        if (result != -1) {
            System.out.println("success");
        } else {
            System.out.println("fail");
        }
    }

    @Test
    public void testDelVideo() {
        VideoCommentDao videoDao = new VideoCommentDaoImpl();
        VideoComment videoComment = new VideoComment(3);
        int result = videoDao.delVideoComment(videoComment);
        if (result != -1) {
            System.out.println("success");
        } else {
            System.out.println("fail");
        }
    }
    @Test
    public void querayAllVideo() {
        VideoCommentService videoCommentService = new VideoCommentServiceImpl();
         List<VideoComment> list = videoCommentService.queryAllComments(5);
        for (int i = 0; i < list.size(); i++) {
            System.out.println(list.get(i));
        }
    }
}
